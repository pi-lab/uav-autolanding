#include "UAVImpl.h"
#include <base/Svar/Svar.h>
#include <base/time/Global_Timer.h>
#include <base/time/Time.h>
#include <base/types/Random.h>
#include <GL/gl.h>
#include <gui/gl/glHelper.h>

#define PI          (3.1415926)
#define D2R         (3.1415926 / 180.0)
#define R2D         (180.0/3.1415926)
#define M2FT        (0.3048)                //transfer (m/sec^2) to (ft/sec^2)
#define R_EARTH     (6378100.0)
#define G_GROUND    (9.8)

UAVImpl::UAVImpl():state(-1)
{

}

void UAVImpl::draw()
{
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);

    glMultMatrix(getPose());
    if(state<0) return;
    // draw the uav here
    const float length=0.3;
    glDisable(GL_LIGHTING);
    glLineWidth(2.5);
    glBegin(GL_LINES);
    glColor3ub(255,0,0);glVertex3f(0,0,0);glVertex3f(length,0,0);
    glColor3ub(0,255,0);glVertex3f(0,0,0);glVertex3f(0,length,0);
    glColor3ub(0,0,255);glVertex3f(0,0,0);glVertex3f(0,0,length);
    glEnd();
    // consider to draw the wind, this may be cool and funny, haha~

    glPopMatrix();
}

void UAVVirtual::draw()
{
    UAVImpl::draw();
    pi::SE3d pose=getPose();
    glColor3ub(0,255,255);
    glBegin(GL_LINES);
    glVertex(pose.get_translation());
    {
        pi::ReadMutex lock(mutexRemote);
        glVertex(targetW);
    }
    glEnd();
}

UAVVirtual::UAVVirtual()
    :targetW(svar.get_var("UAVVirtual.InitTarget",pi::Point3d(0,0,1))),targetSeted(true),sim_tLast(0),sim_tNow(0),
     jsDeadZone(svar.GetDouble("Joystick.DeadZone",0.001)),
     sim_dragK (svar.GetDouble("UAVVirtual.DragK" , 0.25)),
     windMax(svar.GetDouble("UAVVirtual.WindMax",1.))
{
    state=0;
    windRand=svar.get_var("UAVVirtual.WindRand",pi::Point3d(0.05,0.05,0.002));
    start();
}

UAVVirtual::~UAVVirtual()
{

}

bool UAVVirtual::setTarget(const pi::Point3d &target, bool cameraCoordinate)
{
    pi::WriteMutex lock(mutexRemote);
    if(cameraCoordinate)
    {
        targetW=CameraToBoard*target;
    }
    else targetW=target;
    targetSeted=true;
}

bool UAVVirtual::setRemote(const double& roll,const double& pitch,const double& thrust,const double& yaw)
{
    pi::WriteMutex lock(mutexRemote);
    jsR=roll;jsP=pitch;jsT=thrust;jsY=yaw;
    return true;
}

void UAVVirtual::run()
{
    pi::Rate rate(svar.GetInt("UAVVirtual.Rate",100));
    int& pause=svar.GetInt("Pause");
    while(!shouldStop())
    {
        if(targetSeted) updateRemoteFromTarget();
        updateStateFromRemote();
        rate.sleep();
    }
}

bool UAVVirtual::updateRemoteFromTarget()
{
    stringstream info;
    // The uav should always see the board! How?
    pi::SE3d pose=getPose();
    pi::Point3d motionNeeded=targetW-pose.get_translation();
    pi::Point3d velocityNeeded;
    double distance=motionNeeded.norm();
    if(distance>2) velocityNeeded=motionNeeded*(1./distance);//maxvel: 1.0 m/s
    else velocityNeeded=motionNeeded*0.5;// 10 seconds for 10 meters

    pi::Point3d& V_earth=v.get_translation();//velocity
    pi::Point3d accNeeded = velocityNeeded-V_earth;
    // FIXME: could accelleration change very fast?
    pi::Point3d accChange=accNeeded-a.get_translation();

    // FIXME: air with wind friction estimation, should we consider it?
    pi::Point3d D_earth ;//= -V_earth * V_earth.norm()*sim_dragK;
    pi::Point3d G_earth = pi::Point3d(0, 0, -G_GROUND);         //gravity
    pi::Point3d ThrustNeeded = accNeeded-(D_earth+G_earth);


    pi::Point3d ThrustApply;// FIXME: the real thrust uav can take, how to decide this?
    pi::Point3d axisZneeded = ThrustNeeded.normalize();
    double      ThrustNum   = ThrustNeeded.norm();
    if(axisZneeded.z<0) return false;//never drop down!!
    if(axisZneeded.z<cos(0.524))//should no more than 30 degree
    {
        axisZneeded=pi::Point3d(axisZneeded.x*axisZneeded.z,axisZneeded.y*axisZneeded.z,axisZneeded.z);
    }
    if(ThrustNum>2*G_GROUND) ThrustNum=2*G_GROUND;
    ThrustApply=axisZneeded.normalize()*ThrustNum;// most easy, give all what you want

    info<<"V_earth:"<<V_earth<<",accNeeded:"<<accNeeded<<",wind:"<<windVelo
       <<",ThrustNeeded:"<<ThrustNeeded<<",ThrustApply:"<<ThrustApply<<endl;

    // convert 3d thrust to body coordinate and decide the rotation needed, wish to keep yaw not changed
    pi::SO3d keepsYaw;
    double yaw=pose.get_rotation().getYaw();
    keepsYaw.FromEuler(0,yaw,0);
    double R[9];
    keepsYaw.getMatrix(R);
    pi::Point3d vecXYaw(R[0],R[3],R[6]);
    pi::Point3d axisY=ThrustApply.cross(vecXYaw).normalize();
    pi::Point3d axisX=axisY.cross(ThrustApply).normalize();
    pi::Point3d axisZ=ThrustApply.normalize();
    R[0]=axisX.x;R[1]=axisY.x;R[2]=axisZ.x;
    R[3]=axisX.y;R[4]=axisY.y;R[5]=axisZ.y;
    R[6]=axisX.z;R[7]=axisY.z;R[8]=axisZ.z;
    keepsYaw.fromMatrix(R);

//    cout<<info.str()<<endl;
    setRemote(keepsYaw.getRoll(),keepsYaw.getPitch(),ThrustNum/G_GROUND,keepsYaw.getYaw()-yaw);

}

bool UAVVirtual::updateStateFromRemote()
{
    stringstream info;

    info<<"Remote R:"<<jsR<<",P:"<<jsP<<",T:"<<jsT<<",Y:"<<jsY<<endl;
    double      dt;
    // time
    sim_tNow = pi::tm_getTimeStamp();
    dt = sim_tNow - sim_tLast;
    if( dt > 100 ) {
        sim_tLast = sim_tNow;//first time
        return -1;
    }
    sim_tLast = sim_tNow;

    // process joystick values
    if( jsR > -jsDeadZone && jsR < jsDeadZone ) jsR = 0;
    if( jsP > -jsDeadZone && jsP < jsDeadZone ) jsP = 0;
    if( jsT > -jsDeadZone && jsT < jsDeadZone ) jsT = 0;
    if( jsY > -jsDeadZone && jsY < jsDeadZone ) jsY = 0;


    // calculate roll, pitch, yaw
    // rotation matrix of Euler
    // FIXME: angle need to be processed
    pi::SE3d  lastPose=getPose();
    pi::SO3d& lastR   =lastPose.get_rotation();
    double _yaw, _roll, _pitch;

    _roll  = jsR;//30.0 * jsR * D2R;
    _pitch = jsP;//30.0 * jsP * D2R;
//    _yaw   = lastR.getYaw() * D2R;
    _yaw   = lastR.getYaw()+jsY*dt;//30.0 * jsY * D2R*dt;


    pi::SE3d  curPose=lastPose;
    pi::SO3d& curR   =curPose.get_rotation();
    curR.FromEuler(_pitch, _yaw, _roll);

    //Update velocity and position
    pi::Point3d& V_earth=v.get_translation();//last velocity

    windVelo=windVelo+pi::Point3d(pi::Random::RandomGaussianValue(0.,windRand.x),
                                  pi::Random::RandomGaussianValue(0.,windRand.y),
                                  pi::Random::RandomGaussianValue(0.,windRand.z));
    double windScale=windVelo.norm();
    if(windScale>windMax) windVelo=windVelo*(windMax/windScale);
    pi::Point3d V_air   = (V_earth+windVelo);
    pi::Point3d D_earth = -V_air * V_air.norm()*sim_dragK;  //friction from air
    pi::Point3d G_earth = pi::Point3d(0, 0, -G_GROUND);         //gravity
    pi::Point3d T_earth = curR * pi::Point3d(0.0,0.0,double(G_GROUND*(jsT)));//thrust

    // calc  A_earth
    pi::Point3d A_earth = (T_earth + G_earth + D_earth);//D_earth: friction from air, G_earth: gravity, T_earth: thrust
    a.get_translation()=A_earth;
    info<<"T:"<<T_earth<<",G:"<<G_earth<<",D:"<<D_earth<<",A:"<<A_earth<<endl;

    // calc  V_earth
    V_earth  = V_earth + A_earth*dt;
    double v_norm=V_earth.norm();
    if(v_norm>10)// too fast
        V_earth=V_earth*(1./v_norm);

    // calculate x,y,z offset in earth frame
    pi::Point3d& position=curPose.get_translation();
    position=lastPose.get_translation()+V_earth*dt;
    // FIXME: need DEM data to prevent below ground
    if(position.z < 0 ) position.z = 0;
    setPose(curPose);
    info<<"V_earth:"<<V_earth<<",Pose:"<<curPose<<endl;
//    cout<<info.str()<<endl;
    return true;
}



