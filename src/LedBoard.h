#ifndef LEDBOARD_H
#define LEDBOARD_H
#include  <opencv2/core/core.hpp>
#include  <vector>

#include  <gui/gl/GL_Object.h>
#include  <base/types/SPtr.h>

class LedBoard:public std::vector<cv::Point3f>
{
public:
    LedBoard();
    cv::Mat toMat();
    virtual void draw();
    static SPtr<LedBoard> instance();

    double& ledRadius;
};

class LedBoard5:public LedBoard
{
public:
    LedBoard5();
    LedBoard5(cv::Mat LED_Position);
};

class LedBoard5Ideal:public LedBoard
{
public:
    LedBoard5Ideal();
    ~LedBoard5Ideal();
    virtual void draw();

private:
    uint texture;
};

class CameraRect:public pi::gl::GL_Object
{
public:
    CameraRect(std::vector<cv::Point3f>& pts):p(pts){}
    void draw();

    std::vector<cv::Point3f> p;
};

class Grid:public pi::gl::GL_Object
{
public:
    Grid(double _step=1.,int _size=10)
        :step(_step),size(_size){}
    virtual void draw();
    double step;
    int size;
};
#endif // LEDBOARD_H
