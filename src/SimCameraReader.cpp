#include "SimCameraReader.h"
#include "LedBoard.h"
#include "UAV.h"

#include <gui/gl/Win3D.h>
#include <base/time/Global_Timer.h>
#include <base/time/Time.h>
#include <base/Svar/Svar.h>
#include <deque>

#include <opencv2/imgproc/imgproc.hpp>

cv::Mat imgfromQt(QImage in)
{
    in=in.convertToFormat(QImage::Format_RGB888);

    if(in.format()==QImage::Format_RGB32)
    {
        cv::Mat inmat(in.height(),in.width(),CV_8UC4,in.bits());
        return inmat.clone();
    }
    else if(in.format()==QImage::Format_RGB888)
    {
        cv::Mat inmat(in.height(),in.width(),CV_8UC3,in.bits());
        cv::Mat result;
        cv::cvtColor(inmat,result,CV_RGB2GRAY);
        return result;
    }
    else
        cout<<"Can't convert "<<in.format()<<" format Qimage to Mat!\n";
    return cv::Mat();
}

class SimCamera:public pi::gl::Win3D
{
public:
    SimCamera(SPtr<UAV> _uav,pi::hardware::Camera camera):uav(_uav)
    {
        pi::Point2d cxy=camera.Project(pi::Point3d(0,0,1));
        pi::Point2d fxy=camera.Project(pi::Point3d(1,1,1))-cxy;
        setCamera(640,480,fxy.x,fxy.y,cxy.x,cxy.y);
        camera2UAV.get_rotation().FromAxis(pi::Point3d(1,0,0),3.1415926);
        ledboard=LedBoard::instance();
        if(svar.GetInt("SimCamera.Show",0))  show();
    }

    virtual void initializeGL()
    {
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHTING);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_COLOR_MATERIAL);

        // Default colors
        setForegroundColor(QColor(svar.GetString("Win3D.ForegroundColor","#B4B4B4").c_str()));
        setBackgroundColor(QColor("#333333"));

        // Clear the buffer where we're going to draw
        if (format().stereo())
        {
            glDrawBuffer(GL_BACK_RIGHT);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glDrawBuffer(GL_BACK_LEFT);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        else
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Calls user defined method. Default emits a signal.
        init();

        // Give time to glInit to finish and then call setFullScreen().
        if (isFullScreen())
            QTimer::singleShot( 100, this, SLOT(delayedFullScreen()) );
    }


    virtual void draw()
    {
        pi::timer.enter("SimCamera::draw");
        pi::SE3d cameraPose=uav->getPose()*camera2UAV;
        double   timestamp=pi::tm_getTimeStamp();
        this->setPose(cameraPose);
        pi::gl::Win3D::draw();
        ledboard->draw();
        if(frames.size()<2&&qframes.size()<2)
        {
            glFlush();
            QImage qimg=grabFrameBuffer(true);

            if(0)
            {
                qframes.push_back(make_pair(timestamp,qimg));
            }
            else
            {
                SPtr<VideoFrame> frame(new VideoFrame);
                frame->timestamp=timestamp;
                frame->img=imgfromQt(qimg);
                frame->groundPose=cameraPose;
                frames.push_back(frame);
            }
        }
        pi::timer.leave("SimCamera::draw");
    }

    pi::SE3d camera2UAV;
    SPtr<UAV> uav;
    std::deque<pair<double,QImage> > qframes;
    std::deque<SPtr<VideoFrame> > frames;
    SPtr<LedBoard> ledboard;
};

SimCameraReader::SimCameraReader(SPtr<UAV> _uav,pi::hardware::Camera camera)
    :impl(new SimCamera(_uav,camera))
{

}

bool SimCameraReader::valid()
{
    return true;
}

int SimCameraReader::grabImage(VideoFrame& videoframe)
{
    if(!impl.get()) return -1;
    impl->update();
    if(impl->frames.size()<=0) return -1;

    SPtr<VideoFrame> frame=impl->frames.front();
    impl->frames.pop_front();
    videoframe.timestamp=frame->timestamp;
    if(frame->img.channels()==4)
        cv::cvtColor(frame->img,videoframe.img,CV_RGBA2GRAY);
    else
        videoframe.img=frame->img;
    if(!frame->img_gray.empty()) videoframe.img_gray=frame->img_gray;
    else
    {
        if(videoframe.img.type()==CV_8UC3)
            cv::cvtColor(videoframe.img,videoframe.img_gray,CV_BGR2GRAY);
        else videoframe.img_gray=videoframe.img.clone();
    }
//    cout<<"ImageGray.type:"<<videoframe.img_gray.type()<<",C:"<<videoframe.img_gray.channels()
//       <<",E:"<<videoframe.img_gray.elemSize()<<endl;
    return 0;
}
