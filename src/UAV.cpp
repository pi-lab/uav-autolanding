#include "UAV.h"
#include "UAVImpl.h"
#include <base/Svar/Svar.h>
#include <base/Svar/Scommand.h>

using namespace std;

void UAVHandle(void* ptr, std::string sCommand, std::string sParams)
{
    UAV* uav=(UAV*)ptr;
    string cmd;
    stringstream para(sParams);
    para>>cmd;
    if(cmd=="setTarget")
    {
        pi::Point3d target;
        para>>target.x>>target.y>>target.z;
        cout<<"Setting world target "<<target<<endl;
        uav->setTarget(target,false);
    }
    else if(cmd=="setCameraTarget")
    {
        pi::Point3d target;
        para>>target.x>>target.y>>target.z;
        cout<<"Setting camera target "<<target<<endl;
        uav->setTarget(target,true);
    }
    else if(cmd=="setState")
    {
        int mode;
        para>>mode;
        uav->setState(mode);
    }
}

UAV::UAV(const std::string& uav_name):impl(new UAVImpl())
{
    string type=svar.GetString(uav_name+".Type","NoUAV");
    if(type=="Virtual") impl=SPtr<UAVImpl>(new UAVVirtual());
    else if(type=="INNNO") impl=SPtr<UAVImpl>(new UAVINNNO());
    cout<<"UAV: "<<uav_name<<" created "<<((impl->valid())?"Success.":"Failed.");
    if(impl->valid())
    {
        scommand.RegisterCommand(uav_name,UAVHandle,this);
    }
}


void UAV::draw()
{
    impl->draw();
}

bool UAV::setTarget(const pi::Point3d& target,bool cameraCoordinate)
{
    return impl->setTarget(target,cameraCoordinate);
}

bool UAV::setRemote(const double &roll, const double &pitch, const double &thrust, const double &yaw)
{
    return impl->setRemote(roll,pitch,thrust,yaw);
}

std::string  UAV::type()
{
    return impl->Type();
}

bool         UAV::valid()
{
    return impl->valid();
}

int          UAV::state()
{
    return impl->state;
}

pi::SE3d     UAV::getPose()
{
    return impl->getPose();
}

bool  UAV::setState(int state)
{
    impl->state=state;
    return true;
}
