#ifndef VIDEOREADER_H
#define VIDEOREADER_H
#include "VideoFrame.h"
#include <opencv2/highgui/highgui.hpp>
#include <base/types/SPtr.h>


class VideoReader
{
public:
    VideoReader(){}
    virtual ~VideoReader(){}
    virtual bool valid()=0;
    virtual int grabImage(VideoFrame& videoframe)=0;
};

class VideoReaderOpencv:public VideoReader
{
public:
    VideoReaderOpencv(const std::string& filename);
    VideoReaderOpencv(int video);
    virtual ~VideoReaderOpencv();

    virtual int grabImage(VideoFrame& videoframe);
    virtual bool valid(){return cap.isOpened();}

    cv::VideoCapture cap;
};


//#define VIDEO_CAMPE_CV
#ifdef VIDEO_CAMPE_CV
class Video_CAMPE_CV : public VideoReader
{
public:
    Video_CAMPE_CV()
    {
        Camera.set(CV_CAP_PROP_FORMAT, CV_8UC1);
        Camera.set(CV_CAP_PROP_FRAME_WIDTH,  640);
        Camera.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
        Camera.set(CV_CAP_PROP_GAIN, 100);
        Camera.set(CV_CAP_PROP_EXPOSURE, 1);

        if(!Camera.open()) cout<<"Can't open video!\n";
        else cout<<"Video opened success!\n";
    }

    virtual ~Video_CAMPE_CV()
    {
        cout<<"Stop camera..."<<endl;
        Camera.release();
    }

    int write2File(cv::Mat& img_gray)
    {

        string video2save = "Video_File2Save.avi";
        double fps = 30;

        if(video2save!="")
        {

            static cv::VideoWriter video_out(video2save,CV_FOURCC('M', 'J', 'P', 'G'),fps,
                                             cv::Size(img_gray.cols,img_gray.rows),true);
            if(!video_out.isOpened())
            {
                video_out.open(video2save,CV_FOURCC('M', 'J', 'P', 'G'),fps,
                               cv::Size(img_gray.cols,img_gray.rows),true);
                cout<<"Can't open videofile "<<video2save<<" for save.\n";

            }
            video_out<<img_gray;

        }
    }

    virtual int grabImage(VideoFrame& videoframe)
    {
         cout<<"Capturing frames ...."<<endl;
           Camera.grab();
           Camera.retrieve ( videoframe.img_gray);
        //cout<<"Stop camera..."<<endl;
        /////===========================================================================//////
        write2File(videoframe.img_gray);
        return 0;
    }

    raspicam::RaspiCam_Cv Camera;
   // cv::Mat image;
};
#endif

#if 0
class VideoReaderV4L : public VideoReader
{
public:
    VideoReaderV4L(string filename):pvb(NULL)
    {
#if 0
        using namespace CVD;
        cout<<"Openning V4L Video "<<filename<<endl;
        string QuickCamFile =filename;
        ImageRef irSize=svar.get_var<ImageRef>("Video.Size",ImageRef(1920,1080));
        int nFrameRate =  30;
        pvb = new V4LBuffer<yuv422>(QuickCamFile, irSize, -1, false, nFrameRate);

        if(!pvb) cout<<"Can't open video!\n";
        else cout<<"Video opened success!\n";
#endif
    }

    virtual ~VideoReaderV4L()
    {
        delete pvb;
    }

    int write2File(cv::Mat& img)
    {
        string video2save;//=svar.GetString("Video.File2Save","");
        double fps=30;//svar.GetInt("Video.fps",30);
        if(video2save!="")
        {

            static cv::VideoWriter video_out(video2save,CV_FOURCC('M', 'J', 'P', 'G'),fps,
                                             cv::Size(img.cols,img.rows),true);
            if(!video_out.isOpened())
            {
                video_out.open(video2save,CV_FOURCC('M', 'J', 'P', 'G'),fps,
                               cv::Size(img.cols,img.rows),true);
                cout<<"Can't open videofile "<<video2save<<" for save.\n";

            }
            video_out<<img;

        }
    }

    virtual int grabImage(VideoFrame& videoframe)
    {
        using namespace CVD;
        if(!pvb) return -1;
        ImageRef irSize = pvb->size();

        Image<Rgb<byte> > imRGB;
        imRGB.resize(irSize);
        CVD::VideoFrame<yuv422> *pVidFrame = pvb->get_frame();

        videoframe.timestamp=pVidFrame->timestamp();
        convert_image(*pVidFrame, imRGB);
        pvb->put_frame(pVidFrame);
        cv::cvtColor(cv::Mat(irSize.y,irSize.x,CV_8UC3,imRGB.data()),videoframe.img,CV_BGR2RGB);
        write2File(videoframe.img);
        return 0;
    }

    string PathTop;
    CVD::V4LBuffer<CVD::yuv422>* pvb;
};
#endif

#endif // VIDEOREADER_H
