#include "Tracker.h"
#include "ContourImpl.h"
#include "DirectTracker.h"

Tracker::Tracker(TrackMode mode)
{
    if(mode==Contour) impl=SPtr<TrackerImpl>(new ContourImpl);
    if(mode==Direct) impl=SPtr<TrackerImpl>(new DirectTracker);
}

int Tracker::track(VideoFrame& frame)
{
    return impl->track(frame);
}
