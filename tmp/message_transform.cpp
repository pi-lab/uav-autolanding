#include "message_transform.h"


int Message_Transform::start(const char *IP)
{
    if(Is_UART){
        my_UART->open();
    }

    if(Is_SOCKET){
         my_SOCKET->start_client(IP);;
    }

    return 0;
}

int Message_Transform::UDP_Send(pi::SE3d &current)
{
    double message[8] = {
            current.get_rotation().x,
            current.get_rotation().y,
            current.get_rotation().z,
            current.get_rotation().w,
            current.get_translation().x,
            current.get_translation().y,
            current.get_translation().z,
            0
    };
    my_SOCKET->Sendto(message);
    return 0;
}

int Message_Transform::UART_Send(pi::SE3d &current)
{
    flight_cmd = new flight_command(current,0.1);
    cv::Point3d next_step;
    flight_cmd->generate_next_step(next_step);

    // method No.1
    //double Next_Step[3] ={next_step.x,next_step.y,next_step,z};
    //my_UART->write(Next_Step,sizeof(double)*3);

    // method No.2
    my_UART->write(&next_step.x,sizeof(double));
    my_UART->write(&next_step.y,sizeof(double));
    my_UART->write(&next_step.z,sizeof(double));

    return 0;
}

int Message_Transform::UDP_Rev(pi::SE3d &current)
{

}

int Message_Transform::UART_Rev(pi::SE3d &current)
{

}
