#ifndef SIMULATION_H
#define SIMULATION_H
#include <base/types/SPtr.h>
#include <gui/gl/Win3D.h>
#include <gui/gl/SignalHandle.h>
#include <hardware/Camera/Cameras.h>

class UAV;
class LedBoard;

class Simulation:public pi::gl::EventHandle,public pi::gl::Draw_Opengl
{
public:
    Simulation(SPtr<pi::gl::Win3D> _win3d,
               SPtr<LedBoard> _ledboard=SPtr<LedBoard>(),
               pi::hardware::Camera   _camera=pi::hardware::Camera("Camera"),
               SPtr<UAV>      _uav=SPtr<UAV>());

    void run();

    virtual void Draw_Something();
    virtual bool KeyPressHandle(void* arg);
    void setPinholeCamera(int w,int h,double fx,double fy,double cx,double cy);

protected:
    SPtr<pi::gl::Win3D>     win3d;
    SPtr<LedBoard>          ledboard;
    pi::hardware::Camera    camera;
    SPtr<UAV>               uav;
    bool                    shouldStop;
    pi::Point3d             target;
    double left,right,top,bottom,near,far;
};

#endif // SIMULATION_H
