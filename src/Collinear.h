#ifndef POINTS_H
#define POINTS_H
#include <iostream>
#include <vector>
#include <algorithm>

template <typename T>
class __point
{
public:
    __point()
    {
        x = 0;
        y = 0;
    }
    __point(T a,T b)
    {
        x = a;
        y = b;
    }
    __point(const __point<T>& p)
    {
        x = p.x;
        y = p.y;
    }

   const __point<T>& operator-=(const __point<T>& p)
   {
       x -= p.x;
       y -= p.y;
   }

   const __point<T>& operator+=(const __point<T>& p)
   {
       x += p.x;
       y += p.y;
   }

    const T& get_x()
    {
        return x;
    }
    const T& get_y()
    {
        return y;
    }

public:
    friend __point<T> operator-(const __point<T>& p2, const __point<T>& p1){
        __point<T> p_res(p1);
        p_res -= p2;
        return p_res;
    }
    friend __point<T> operator+(const __point<T>& p2, const __point<T>& p1){
        __point<T> p_res(p1);
        p_res += p2;
        return p_res;
    }



protected:
    T x;
    T y;
};

//#define    int_4Bit        char

typedef __point<int>       IPoint;
typedef __point<float>     FPoint;
typedef __point<double>    DPoint;
typedef __point<short>     SIPoint;
//typedef __point<int_4Bit>  4BitPoint;

template<typename T>
void isCollinear_1(__point<T> &p1, __point<T> &p2,__point<T>& p3,int& index);

bool isCollinear_0(std::vector<FPoint> &point,std::vector<int> &index);

int isCollinear(std::vector<FPoint> &p1,std::vector<int> &index);

bool isCollinear_0(std::vector<FPoint> &point,std::vector<int> &index)
{
    bool found=false;
  // std::cout<<"doing is isCollinear_0\n";
//    令a = sqrt( 1 / ((y2-y1)^2 + (x2-x1)^2) ),
//    fabs( a*(x2-x1) * y3 - a*(y2-y1) * x3 - a*y1*x2 + a*x1*y2 ) < 1e-6
   float collinear_min=18;
   for(int i=0;i<point.size()-2;i++)
     for(int j=i+1;j<point.size()-1;j++)
       for(int k=j+1;k<point.size();k++)
         {
            FPoint p1=point[i],  p2=point[j], p3=point[k];
            float collinear=sqrt(1./(      ( p2.get_y()-p1.get_y() )*( p2.get_y()-p1.get_y() )
                                   +   ( p2.get_x()-p1.get_x() )*( p2.get_x()-p1.get_x() )
                                )
                            );

            float collinear_control=fabs(   collinear*(p2.get_x()-p1.get_x())*p3.get_y()
                         - collinear*(p2.get_y()-p1.get_y())*p3.get_x()
                         - collinear*p1.get_y() *p2.get_x()
                         + collinear*p1.get_x() *p2.get_y() );

       // std::cout<<"collinear_control "<<collinear_control<<"\n";
            if(collinear_control<18)
            {
                found=true;
                if(collinear_min>collinear_control)
                {
                    collinear_min=collinear_control;
                    index[0]=i;
                    index[1]=j;
                    index[2]=k;
                   // std::cout<<"found=true and First:  "<<index[0]<<","<<index[1]<<","<<index[2]<<"\n";


                }
            }

        }
   return found;
}
template<typename T>
void isCollinear_1(__point<T> &p1, __point<T> &p2,__point<T>& p3,int& index){
   //  std::cout<<"doing is isCollinear_1\n";
        T x12 = abs(p1.get_x() - p2.get_x());
        T x23 = abs(p2.get_x() - p3.get_x());
        T x13 = abs(p1.get_x() - p3.get_x());
       // T max_x= x12>x23 ? x12:x23;
        std::vector< std::pair<T,int> > val_x;
        val_x.push_back(std::make_pair(x12,12));
        val_x.push_back(std::make_pair(x23,23));
        val_x.push_back(std::make_pair(x13,13));
        std::sort(val_x.begin(),val_x.end());

        T y12 = abs(p1.get_y() - p2.get_y());
        T y23 = abs(p2.get_y() - p3.get_y());
        T y13 = abs(p1.get_y() - p3.get_y());

        std::vector<std::pair<T,int> > val_y;
        val_y.push_back(std::make_pair(y12,12));
        val_y.push_back(std::make_pair(y23,23));
        val_y.push_back(std::make_pair(y13,13));
        std::sort(val_y.begin(),val_y.end());
        int max_x = val_x[2].first>val_y[2].first?val_x[2].second:val_y[2].second;

        switch (max_x) {
        case 12:
            index=3;
            break;
        case 13:
            index=2;
            break;
        default:
            index=1;
            break;
        }
}




int isCollinear(std::vector<FPoint> &point, std::vector<int> &index)
{
  //  std::cout<<"doing is collinear\n";
    int num_index[2][2];
    int midle;
    std::vector<int> first_index;
    first_index.resize(3);

    if(isCollinear_0(point,first_index))
    {
//        std::cout<<first_index[0]<<","<<first_index[1]<<","<<first_index[2]<<"\n";
         isCollinear_1(point[first_index[0]],point[first_index[1]],point[first_index[2]],midle);

         switch (midle) {
                    case 1:
                        index[1]=first_index[0];
                        num_index[0][0]=first_index[1];
                        num_index[0][1]=first_index[2];
                        break;
                    case 2:
                        index[1]=first_index[1];
                        num_index[0][0]=first_index[0];
                        num_index[0][1]=first_index[2];
                        break;
                    default:
                        index[1]=first_index[2];
                        num_index[0][0]=first_index[0];
                        num_index[0][1]=first_index[1];
                        break;
                     }

                   FPoint p1;

                    for(int num=0,p=0;num<5;num++)
                    {
                        if(num!=first_index[0]&&num!=first_index[1]&&num!=first_index[2]){
                            p1 += point[num];
                            num_index[1][p]=num;
                            p++;
                        }
                    }

#define Y_Y point[index[1]].get_y()
#define X_X point[index[1]].get_x()

                    FPoint p2(p1.get_x()/2.,p1.get_y()/2.);
                    FPoint p3(p2.get_x()-X_X,p2.get_y()-Y_Y);

                    FPoint p4(point[num_index[1][0]].get_x()-X_X,
                              point[num_index[1][0]].get_y()-Y_Y);

                    FPoint p5(point[num_index[0][0]].get_x()-X_X,
                              point[num_index[0][0]].get_y()-Y_Y);

                    if((p3.get_x()*p4.get_y()-p4.get_x()*p3.get_y())>0)
                    {
                        index[4]=num_index[1][0]; index[3]=num_index[1][1];
                    }
                    else
                    {
                        index[4]=num_index[1][1]; index[3]=num_index[1][0];
                    }

                    if((p3.get_x()*p5.get_y()-p5.get_x()*p3.get_y())>0)
                    {
                        index[2]=num_index[0][0]; index[0]=num_index[0][1];
                    }
                    else
                    {
                        index[2]=num_index[0][1]; index[0]=num_index[0][0];
                    }
                    return 0;
     }
    return -1;
}

#endif // 3POINTS_H

