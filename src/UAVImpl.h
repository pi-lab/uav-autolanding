#ifndef UAVIMPL_H
#define UAVIMPL_H

#include <base/types/SE3.h>
#include <base/system/system.h>

class UAVImpl
{
    friend class UAV;
public:
    UAVImpl();
    ~UAVImpl(){}

    virtual bool setTarget(const pi::Point3d& target,bool cameraCoordinate=true){return false;}

    virtual bool setRemote(const double& roll,const double& pitch,const double& thrust,const double& yaw){return false;}

    virtual std::string Type(){return "NoUAV";}

    pi::SE3d getPose(){pi::ReadMutex lock(mutexPose);return CameraToBoard;}
    void     setPose(const pi::SE3d& pose){pi::WriteMutex lock(mutexPose);CameraToBoard=pose;}

    bool valid(){return state>=0;}

    virtual void draw();
protected:

    pi::SE3d         CameraToBoard,a,v;
    pi::MutexRW      mutexPose;
    int      state;
};

class UAVVirtual:public UAVImpl,public pi::Thread
{
public:
    UAVVirtual();
    ~UAVVirtual();
    virtual bool setTarget(const pi::Point3d &target, bool cameraCoordinate=true);
    virtual bool setRemote(const double& roll,const double& pitch,const double& thrust,const double& yaw);

    virtual std::string Type(){return "Virtual";}

    virtual void draw();

private:
    void    run();
    bool    updateRemoteFromTarget();
    bool    updateStateFromRemote();

    // remote state
    pi::Point3d targetW;
    bool        targetSeted;
    double jsR,jsP,jsT,jsY,&jsDeadZone;//remote values
    pi::MutexRW mutexRemote;

    // sim
    double &sim_dragK,windMax;
    pi::Point3d windVelo,windRand;

    double sim_tNow,sim_tLast;//simulate time
};

class UAVINNNO:public UAVImpl
{
public:
    virtual bool setTarget(const pi::Point3d &target, bool cameraCoordinate=true){return false;}
    virtual bool setRemote(const double& roll,const double& pitch,const double& thrust,const double& yaw){return false;}

    virtual std::string Type(){return "INNNO";}

};

#endif // UAVIMPL_H
