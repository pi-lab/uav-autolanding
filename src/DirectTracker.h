#ifndef DIRECTTRACKER_H
#define DIRECTTRACKER_H
#include "TrackerImpl.h"
#include <base/types/SPtr.h>
#include "LedBoard.h"
#include <hardware/Camera/Cameras.h>

struct WeightedPoint
{
    WeightedPoint(int _x,int _y,double _weight)
        :x(_x),y(_y),weight(_weight){}
    int x,y;
    double weight;
};

struct WeightedCircle
{
    enum WeightedType{Huber=0,Turkey=1,WELSCH=2};
public:
    WeightedCircle(double radius=10.,int type=Huber);
    void normalize();

    std::vector<WeightedPoint> pts;
    double inv_size;
};

class DirectTracker:public TrackerImpl
{
public:
    DirectTracker();
    ~DirectTracker();

    virtual int track(VideoFrame& frame);
    int trackLines();
    int trackCircle();
    int trackWeightedCircle();
    int trackSmallDirect();

public:
    void errorFunc(double *para, double *err, int n_para, int n_err);
    void errorLines(double *para, double *err, int n_para, int n_err);
    void errorCircle(double *para, double *err, int n_para, int n_err);
    void errorWeightedCircle(double *para, double *err, int n_para, int n_err);
    void errorSmallDirect(double *para, double *err, int n_para, int n_err);

    SPtr<LedBoard>          ledboard;
    pi::hardware::Camera    camera;
    pi::SE3d                pose;
    pi::Array_<double,6>    p;
    double                  error;
    std::vector<double>     errorVec;

    pi::Point2d             pmin,pmax,pnow;
    int                     size,count;
    std::vector<pi::Point3d> ptsW;
    double opts[5], info[10];
    SPtr<WeightedCircle>    weightedCircle;

    VideoFrame*             frame;

    cv::Mat                 frameImageSmall;
    int&                    method,&verbose,&win3d,&blurSize;
    double&                 smallDirectScale;
};

#endif // DIRECTTRACKER_H
