#include "Simulation.h"
#include "LedBoard.h"
#include "UAV.h"
#include "VideoFrame.h"
#include <deque>

#include <base/Svar/Svar.h>
#include <base/time/Global_Timer.h>
#include <base/time/Time.h>

Simulation::Simulation(SPtr<pi::gl::Win3D> _win3d,
           SPtr<LedBoard> _ledboard,pi::hardware::Camera   _camera, SPtr<UAV>      _uav)
    :win3d(_win3d),ledboard(_ledboard),camera(_camera),uav(_uav),shouldStop(false),target(0,0,10),
      near(-1)
{
    if(!ledboard.get())
    {
        ledboard=LedBoard::instance();
    }
    if(!camera.isValid())
    {
        camera=pi::hardware::Camera(svar.GetString("Camera","CameraPI"));
    }
    if(!uav)
    {
        uav=SPtr<UAV>(new UAV("VirtualUAV"));
    }
}

void Simulation::setPinholeCamera(int w,int h,double fx,double fy,double cx,double cy)
{
    cout<<"Set Pinhole:"<<w<<h<<fx<<fy<<cx<<cy<<endl;
    if(fx<=0||fy<=0||w<=0||h<=0) return ;
    if(win3d.get())
    {
        win3d->setCamera(w,h,fx,fy,cx,cy);
//        near=win3d->camera()->zNear();
//        far=win3d->camera()->zFar();
//        left=-cx/fx*near;
//        right=(w-cx)/fx*near;
//        top=(h-cy)/fy*near;
//        bottom=(-cy)/fy*near;
//        near=-near;
//        far=-far;
    }
}

void Simulation::run()
{
    if(win3d.get())
    {
        win3d->SetDraw_Opengl(this);
        win3d->SetEventHandle(this);
        win3d->setSceneRadius(20);
        win3d->setCamera(640,480,0,0,0,0);
        win3d->setBackgroundColor(QColor(0,0,0));
        win3d->Show();
    }

    pi::Rate rate(100);
    pi::TicTac tictac;
    tictac.Tic();
    int& pause=svar.GetInt("Pause");
    int& cameraFollow(svar.GetInt("Win3d.CameraFollow",1));
    pi::SE3d camera2UAV;
    camera2UAV.get_rotation().FromAxis(pi::Point3d(1,0,0),3.1415926);
    while(!shouldStop)
    {
        if(!pause)
        {
            if(win3d.get()&&tictac.Tac()>0.033)
            {
                if(cameraFollow) win3d->setPose(uav->getPose()*camera2UAV);
                win3d->update();
                tictac.Tic();
            }
            rate.sleep();
        }
    }
}

bool Simulation::KeyPressHandle(void* arg)
{
    QKeyEvent* e=(QKeyEvent*)arg;
    switch(e->key())
    {
    case Qt::Key_Space:
        return false;
    case Qt::Key_Escape:
        shouldStop=true;
        return false;

    case Qt::Key_P:
        return true;

    case Qt::Key_C:
    {
        pi::Point2d cxy=camera.Project(pi::Point3d(0,0,1));
        pi::Point2d fxy=camera.Project(pi::Point3d(1,1,1))-cxy;
        setPinholeCamera(640,480,fxy.x,fxy.y,cxy.x,cxy.y);
    }
        return true;

    case Qt::Key_M:
        svar.GetDouble("Image2Show.Scale",0.5)*=1.2;
        return true;

    case Qt::Key_N:
        svar.GetDouble("Image2Show.Scale",0.5)*=0.8;
        return true;

    default:
        svar.i["KeyPressMsg"] = e->key();
        return false;
    }
}

void Simulation::Draw_Something()
{
    if(near>0)
        glFrustum(left,right,bottom,top,near,far);
    if(uav.get()) uav->draw();
    if(ledboard.get()) ledboard->draw();
}
