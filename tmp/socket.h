#ifndef SOCKET_H
#define SOCKET_H
/*************************************************************************
 > File Name: socket.h
 > Author: SongLee
 ************************************************************************/
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <string.h>
#include <base/types/SE3.h>

#define SERVER_PORT 8000
#define BUFFER_SIZE 200
#define FILE_NAME_MAX_SIZE 512

class Socket_MYUDP
{
public:
    Socket_MYUDP()
    {
        bzero(&socket__addr, sizeof(socket__addr));
        socket__addr.sin_family = AF_INET;
        socket__addr.sin_port = htons(SERVER_PORT);
    }
    ~Socket_MYUDP()
    {
        close(__socket_fd);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief start_server
    ///
    void start_server()
    {
        socket__addr.sin_addr.s_addr = htonl(INADDR_ANY);
        /* 创建socket */
        __socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(__socket_fd == -1)
        {
            perror("Create Socket Failed:");
            exit(1);
        }
        /* 绑定套接口 */
        if(-1 == (bind(__socket_fd,(struct sockaddr*)&socket__addr,sizeof(socket__addr))))
        {
            perror("Server Bind Failed:");
            exit(1);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief start_client
    ///
    int start_client(const char *__IP = "127.0.0.1")
    {
        /* 服务端地址 */
        socket__addr.sin_addr.s_addr = inet_addr(__IP);
        /* 创建socket */
        __socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(__socket_fd < 0)
        {
            perror("Create Socket Failed:");
            exit(1);
        }
        return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Recvfrom
    ///
    int Recvfrom() /* 数据传输 */
    {
        /* 定义一个地址，用于捕获客户端地址 */
        struct sockaddr_in client_addr;
        socklen_t client_addr_length = sizeof(client_addr);

        /* 接收数据 */
        char buffer[BUFFER_SIZE];
        bzero(buffer, BUFFER_SIZE);
        int rec = recvfrom(__socket_fd, buffer, BUFFER_SIZE,0,(struct sockaddr*)&client_addr, &client_addr_length) ;
        if(rec == -1)
        {
            perror("Receive Data Failed:");
            exit(1);
        }

        /* 从buffer中拷贝出file_name */
        double pose[8];
        memcpy(&pose,buffer,sizeof(double)*8); //解析过程
        printf("Roatation=(%f, %f, %f, %f)\nTranslation=(%f,%f,%f)\n error=\n\n"
//               , pose.get_rotation().x
//               , pose.get_rotation().y
//               , pose.get_rotation().z
//               , pose.get_rotation().w
//               , pose.get_translation().x
//               , pose.get_translation().y
//               , pose.get_translation().z);
               ,pose[0],pose[1],pose[2],pose[3],pose[4],pose[5],pose[6],pose[7]);
        return rec;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Sendto
    ///
    int Sendto(double *message)
    {
            char buffer[BUFFER_SIZE];
            bzero(buffer, BUFFER_SIZE);

            memcpy(buffer,message,sizeof(double)*8);
            /* 发送文件名 */
            if(sendto(__socket_fd, buffer, BUFFER_SIZE,0,(struct sockaddr*)&socket__addr,sizeof(socket__addr)) < 0)
            {
                perror("Send File Name Failed:");
                exit(1);
            }
        return 0;
    }
protected:
    /* 创建UDP套接口 */
    struct sockaddr_in socket__addr;
    int __socket_fd;
};

#endif // SOCKET_H
