#include "flight_command.h"




int flight_command::generate_next_step(cv::Point3d &next)
{
   //
    if(height > safe_height){
        if(distance > 1){
            double k = 1 - max_step / distance ;
            next = (cv::Point3d(k*current_pose.get_translation().x,k*current_pose.get_translation().y,k*current_pose.get_translation().z));
        }
        else {
            double k = 1 - max_step / distance ;
            next = (cv::Point3d(k*current_pose.get_translation().x,k*current_pose.get_translation().y,k*current_pose.get_translation().z));
        }
    }

    // height < safe_height , first move on x-y plane ,and then descent height;
    else{
         if(distance > (safe_height+0.1)){
            double xy_distance = sqrt(current_pose.get_translation().x*current_pose.get_translation().x+current_pose.get_translation().y*current_pose.get_translation().y);
            double k = 1 - max_step / xy_distance ;
            next = (cv::Point3d(k*current_pose.get_translation().x,k*current_pose.get_translation().y,current_pose.get_translation().z));
         }
         else if (distance > 0.3){
            double k = 0.8;
            next = (cv::Point3d(k*current_pose.get_translation().x,k*current_pose.get_translation().y,k*current_pose.get_translation().z));
         }
         else {
             double k = 0.4;
             next = (cv::Point3d(k*current_pose.get_translation().x,k*current_pose.get_translation().y,k*current_pose.get_translation().z));
         }
    }

}

//if(current_pose.get_translation().x - landing_position_x > );
