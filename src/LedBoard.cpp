#include "LedBoard.h"
#include <GL/gl.h>
#include <gui/gl/Win3D.h>
#include <base/Svar/Svar.h>
#include <opencv2/highgui/highgui.hpp>
using namespace std;

LedBoard::LedBoard()
    :ledRadius(svar.GetDouble("LedBoard.Radius",0.01))
{

}

void LedBoard::draw()
{
    glDisable(GL_LIGHTING);
    glColor3ub(255,0,0);
    glPointSize(5.5);
    glLineWidth(3);
    glBegin(GL_POINTS);
    for(int i=0,iend=size();i<iend;i++)
        glVertex3f(at(i).x,at(i).y,at(i).z);
    glEnd();
    glBegin(GL_LINES);
    for(int i=0,iend=size();i<iend;i++)
        glVertex3f(at(i).x,at(i).y,at(i).z);
    glEnd();
}


SPtr<LedBoard> LedBoard::instance()
{
    static SPtr<LedBoard> ledboard;
    if(!ledboard.get())
    {
        int LedBoardType=svar.GetInt("LedBoard.Type",1);
        if(LedBoardType==0)
        {
            //load ledboard
            cv::FileStorage fs(svar.GetString("LedBoard.File","test.yam"),
                               cv::FileStorage::READ);
            cv::Mat cameraMatrix,LED_Position;
            fs["cameraMatrix"] >> cameraMatrix;
            fs["LED_Position"] >> LED_Position;
            cout<<"LED_Position = \n"<<LED_Position<<"\n";
            ledboard=SPtr<LedBoard>(new LedBoard5(LED_Position));
        }
        else ledboard=SPtr<LedBoard>(new LedBoard5Ideal());

        SvarWithType<SPtr<LedBoard> >::instance()["LedBoardSPtr"]=ledboard;
        if(ledboard->size()!=5)
        {cerr<<"Not correct LedBoard!";return SPtr<LedBoard>();}
        return ledboard;
    }
    return ledboard;
}

cv::Mat LedBoard::toMat()
{
    return cv::Mat(size(),3,CV_32F,data()).clone();
}

LedBoard5::LedBoard5()
{
}

LedBoard5::LedBoard5(cv::Mat LED_Position)
{
    resize(5);
    for(int i=0;i<5;i++){
        this->at(i)=cv::Point3f(LED_Position.row(i).at<double>(0,0),
                                LED_Position.row(i).at<double>(0,1),
                                LED_Position.row(i).at<double>(0,2));
    }

}

LedBoard5Ideal::LedBoard5Ideal()
    :texture(0)
{
    resize(5);
    this->at(0)=cv::Point3f(0.1,0.1,0);
    this->at(1)=cv::Point3f(0,0.1,0);
    this->at(2)=cv::Point3f(-0.1,0.1,0);
    this->at(3)=cv::Point3f(0.1,-0.1,0);
    this->at(4)=cv::Point3f(-0.1,-0.1,0);
}

LedBoard5Ideal::~LedBoard5Ideal()
{
    if(texture)
    {
        SPtr<pi::gl::Win3D> win3d=SvarWithType<SPtr<pi::gl::Win3D> >::instance()["MainWin3D"];
        if(win3d.get()) win3d->deleteTexture(texture);
    }
}

void LedBoard5Ideal::draw()
{
    GLint last_texture_ID;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture_ID);
    if(!texture)
    {
        if(0)
        {
        SPtr<pi::gl::Win3D> win3d=SvarWithType<SPtr<pi::gl::Win3D> >::instance()["MainWin3D"];
        if(win3d.get())
        texture=win3d->bindTexture(QString::fromStdString
                               (svar.GetString("LedBoard.File","data/ledboard.png")));
        }
        else
        {
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glGenTextures(1, &texture );
            glBindTexture(GL_TEXTURE_2D,texture);
            cv::Mat textureImage=cv::imread(svar.GetString("LedBoard.File","data/ledboard.png"));
            glTexImage2D(GL_TEXTURE_2D, 0,
                         GL_RGB, textureImage.cols,textureImage.rows, 0,
                         GL_BGR, GL_UNSIGNED_BYTE,textureImage.data);
            //            glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
            //            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
            //            glTexEnvfv(GL_TEXUTRE_ENV,GL_TEXTURE_ENV_COLOR,&ColorRGBA);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            //            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }
        if(texture)
        {
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
        }
        else
        {
            std::cerr<<"ErrorBindTexture:"
                    <<svar.GetString("LedBoard.File","data/ledboard.png")<<std::endl;
            return;
        }
    }
    else glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glTexCoord2f(0.,0.);glVertex3f(-0.2,0.2,0);
    glTexCoord2f(1.,0.);glVertex3f(0.2,0.2,0);
    glTexCoord2f(1.,1.);glVertex3f(0.2,-0.2,0);
    glTexCoord2f(0.,1.);glVertex3f(-0.2,-0.2,0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D,last_texture_ID);
}


void CameraRect::draw()
{
    glDisable(GL_LIGHTING);
    if(p.size()!=4) return;
    glLineWidth(1.5);
    glBegin(GL_LINES);
    glVertex3f(p[0].x,p[0].y,p[0].z);
    glVertex3f(0,0,0);
    glVertex3f(p[1].x,p[1].y,p[1].z);
    glVertex3f(0,0,0);
    glVertex3f(p[2].x,p[2].y,p[2].z);
    glVertex3f(0,0,0);
    glVertex3f(p[3].x,p[3].y,p[3].z);
    glVertex3f(0,0,0);
    glVertex3f(p[0].x,p[0].y,p[0].z);
    glVertex3f(p[1].x,p[1].y,p[1].z);
    glVertex3f(p[1].x,p[1].y,p[1].z);
    glVertex3f(p[2].x,p[2].y,p[2].z);
    glVertex3f(p[2].x,p[2].y,p[2].z);
    glVertex3f(p[3].x,p[3].y,p[3].z);
    glVertex3f(p[3].x,p[3].y,p[3].z);
    glVertex3f(p[0].x,p[0].y,p[0].z);
    glEnd();
}

void Grid::draw()
{
    glColor3ub(255,255,0);
    float maxvar=size*step*0.5;
    glBegin(GL_LINES);
    for(int i=0;i<=size;i++)
    {
        glVertex3f(maxvar,maxvar-step*i,0);
        glVertex3f(-maxvar,maxvar-step*i,0);
        glVertex3f(maxvar-step*i,maxvar,0);
        glVertex3f(maxvar-step*i,-maxvar,0);
    }
    glEnd();
}
