#ifndef CONTOURIMPL_H
#define CONTOURIMPL_H
#include "TrackerImpl.h"
#include <base/types/SPtr.h>
#include "LedBoard.h"
#include <hardware/Camera/Cameras.h>

class ContourImpl:public TrackerImpl
{
public:
    ContourImpl();

    virtual int track(VideoFrame& frame);

private:
    int TrackContours(VideoFrame& frame);
    int TrackContoursFast(VideoFrame& frame);
    int FindPoints(VideoFrame& frame);
    int ComputeLocation(VideoFrame& frame);

    int& blurSize,&erodeSize,&dilateSize;
    double& thresh;
    double& disFactor;
    SPtr<LedBoard> ledboard;
    pi::hardware::Camera         camera;
};

#endif // CONTOURIMPL_H
