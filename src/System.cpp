#include "System.h"
#include "SystemImpl.h"

using namespace std;

System::System():impl(new SystemImpl())
{

}

void System::stop()
{
    impl->stop();
    impl->join();
    if(impl->isRunning()) usleep(1000);
    cout<<"System joined.\n";
}
