#include "ContourImpl.h"
#include "Collinear.h"
#include <base/Svar/Svar.h>
#include <base/time/Global_Timer.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

ContourImpl::ContourImpl()
    :blurSize(svar.GetInt("TrackContours.Blur",0)),
      thresh(svar.GetDouble("TrackContours.CannyThreshold",100.)),
      disFactor(svar.GetDouble("TrackContours.DisFactor",1.)),
      erodeSize(svar.GetInt("TrackContours.Erode")),
      dilateSize(svar.GetInt("TrackContours.Dilate"))
{
    ledboard=SvarWithType<SPtr<LedBoard> >::instance()["LedBoardSPtr"];
    if(!ledboard.get())
    {
        cerr<<"No valid ledboard!\n";exit(0);
    }
    camera  =SvarWithType<pi::hardware::Camera>::instance()["MainCamera"];
    if(!camera.isValid())
    {
        cerr<<"No valid camera!\n";
    }
}

int ContourImpl::track(VideoFrame &frame)
{
//    if(0)
        if(TrackContours(frame)<0) return -1;
//    else
//    if(TrackContoursFast(frame)<0) return -1;
    if(FindPoints(frame)<0) return -2;
    return ComputeLocation(frame);
}

int ContourImpl::TrackContoursFast(VideoFrame& frame)
{
    pi::timer.enter("ContourImpl::TrackContoursFast");
    pi::timer.enter("ContourImpl::TrackContoursFast::CollectPoints");
    vector<cv::Point> brightPoints;
    int wh=frame.img_gray.cols*frame.img_gray.rows;
    brightPoints.reserve(wh);
    uchar* p=frame.img_gray.data;
    for(int y=0,yend=frame.img_gray.rows;y<yend;y++)
        for(int x=0,xend=frame.img_gray.cols;x<xend;x++)
        {
            if(*p>100) brightPoints.push_back(cv::Point(x,y));
            p++;
        }
    cout<<"got "<<brightPoints.size()<<" bright points.\n";
    pi::timer.leave("ContourImpl::TrackContoursFast::CollectPoints");

    pi::timer.enter("ContourImpl::TrackContoursFast::CollectContours");
    vector<vector<Point> >& contours=frame.contours;
    while(brightPoints.size())
    {
        vector<cv::Point> unCollected,collected;
        unCollected.reserve(brightPoints.size());
        collected.reserve(brightPoints.size());
        cv::Point pt=brightPoints.front(),dis;
        collected.push_back(pt);
        for(int i=1;i<brightPoints.size();i++)
        {
            dis=brightPoints[i]-pt;
            if(dis.x*dis.x+dis.y+dis.y<100) collected.push_back(brightPoints[i]);
            else unCollected.push_back(brightPoints[i]);
        }
        contours.push_back(collected);
        brightPoints=unCollected;
    }
    frame.state==VideoFrame::ContoursFound;
    pi::timer.leave("ContourImpl::TrackContoursFast::CollectContours");
    pi::timer.leave("ContourImpl::TrackContoursFast");
    return 0;
}

int ContourImpl::TrackContours(VideoFrame& frame)
{
    cv::Mat imgGray=frame.img_gray;
    pi::timer.enter("ContourImpl::TrackContours");

    if(blurSize>1)
    {
        pi::timer.enter("ContourImpl::TrackContours::Blur");
        cv::blur( imgGray,imgGray , Size(blurSize,blurSize) );
        pi::timer.leave("ContourImpl::TrackContours::Blur");
    }

    {
        pi::timer.enter("ContourImpl::TrackContours::threshold");
        uchar* p=imgGray.data;
        uchar  max=*p;
        for(uchar* pend=p+imgGray.cols*imgGray.rows;p<pend;p++) if(*p>max) max=*p;
        cv::threshold(imgGray,imgGray,max*0.5,255,CV_THRESH_BINARY);//CV_THRESH_TOZERO
        //    adaptiveThreshold(frame.img_gray,frame.img_gray,255,0,1,blurSize,blurSize);
        pi::timer.leave("ContourImpl::TrackContours::threshold");
    }

    if(erodeSize)
    {
        pi::timer.enter("ContourImpl::TrackContours::erode");
        cv::Mat element1 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(erodeSize,erodeSize));
        cv::erode(imgGray,imgGray,element1);
        pi::timer.leave("ContourImpl::TrackContours::erode");
    }

    if(dilateSize)
    {
        pi::timer.enter("ContourImpl::TrackContours::dilate");
        cv::Mat element2 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(dilateSize,dilateSize));
        cv::dilate(imgGray,imgGray,element2);
        pi::timer.leave("ContourImpl::TrackContours::dilate");
    }


    if(0)
    {
        pi::timer.enter("ContourImpl::TrackContours::Canny");
        Canny( imgGray, imgGray, thresh, thresh*2, 3 );
        pi::timer.leave("ContourImpl::TrackContours::Canny");
    }

    pi::timer.enter("ContourImpl::TrackContours::findContours");
    vector<vector<Point> >& contours=frame.contours;
    vector<Vec4i> hierarchy;//=frame.hierarchy;
    findContours( imgGray.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(0, 0) );//CV_CHAIN_APPROX_SIMPLE

    if(contours.size())
        frame.state==VideoFrame::ContoursFound;
    pi::timer.leave("ContourImpl::TrackContours::findContours");
    pi::timer.leave("ContourImpl::TrackContours");
    return 0;
}

template <class T>
void findCenterRadius(T& points,Point2f& center,double& radius)
{
    radius=-1;
    if(!points.size())
    {
        return;
    }

    center=Point2f(0,0);
    for(int i=0,iend=points.size();i<iend;i++)
    {
        center=center+Point2f(points[i].x,points[i].y);
    }
    center=Point2f(center.x/points.size(),center.y/points.size());

    for(int i=0,iend=points.size();i<iend;i++)
    {
        Point2f diff=Point2f(points[i].x,points[i].y)-center;
        double  r=diff.ddot(diff);
        if(r>radius) radius=r;
    }
    radius=sqrt(radius);
}

template <class T>
void findCenterRadiusWithWeight(T& points,Point2f& center,double& radius,cv::Mat img)
{
    radius=-1;
    if(!points.size())
    {
        return;
    }

    Point2d center2d(0,0);
    double sumWeight=0;
    double weight;
    for(int i=0,iend=points.size();i<iend;i++)
    {
        weight=img.at<uchar>(points[i]);
        center2d=center2d+Point2d(points[i].x,points[i].y)*weight;
        sumWeight+=weight;
    }
    center=center2d*(1./sumWeight);

    for(int i=0,iend=points.size();i<iend;i++)
    {
        Point2f diff=Point2f(points[i].x,points[i].y)-center;
        double  r=diff.ddot(diff);
        if(r>radius) radius=r;
    }
    radius=sqrt(radius);
}


int ContourImpl::FindPoints(VideoFrame& frame)
{
    // 1. compute centers and radius
    pi::timer.enter("ContourImpl::FindPoints");
    int n=frame.contours.size();
    frame.pointsCenters2d.resize(n);
    frame.pointsRadius2d.resize(n,-1);

    for(int i=0,iend=n;i<iend;i++)
    {
        if(0)
            findCenterRadius(frame.contours[i],frame.pointsCenters2d[i],frame.pointsRadius2d[i]);
        else findCenterRadiusWithWeight(frame.contours[i],frame.pointsCenters2d[i],frame.pointsRadius2d[i],frame.img_gray);
    }

    if(1)
    for( int i = 0; i< n; i++ )
    {
        cv::circle(frame.img, frame.pointsCenters2d[i],2,Scalar(0,0,255),2);
        {
            std::stringstream ss;
            std::string text;
            ss<<i+1;
            ss>>text;
            cv::putText(frame.img,text,frame.pointsCenters2d[i],0,2,Scalar(0,255,255));
        }
    }

    // 2. filt points
    cv::Point2f pointsCenter;
    double      pointsRadius;
    vector<bool> removedFlags(n,false);
    findCenterRadius(frame.pointsCenters2d,pointsCenter,pointsRadius);

   // std::cout<<n<<"\n";
    for(int i=0;i<n-1;i++)
    {
        if(removedFlags[i]) continue;
        for(int j=i+1;j<n;j++)
        {
            if(removedFlags[j]) continue;
            cv::Point2f e=frame.pointsCenters2d[i]-frame.pointsCenters2d[j];
            float dis=sqrt(e.dot(e));

            //should we remove one of them?
            bool crossed=dis*2<frame.pointsRadius2d[i]+frame.pointsRadius2d[j];
            if(crossed  || dis<pointsRadius*0.1)
            {
//                cout<<"i = "<<i+1<<", j = "<<j+1<<"\n";
//                cout<<"crossed="<<crossed<<", dis="<<dis<<", pointsRadius*0.1="<<pointsRadius*0.1<<"\n";
                //witch of them to remove?
//                if(0){
//                    cout<<"["<<i<<","<<j<<"],Dis:"<<dis<<endl;
//                    cout<<"P1:"<<frame.pointsCenters2d[i]<<",R1:"<<frame.pointsRadius2d[i]<<endl;
//                    cout<<"P2:"<<frame.pointsCenters2d[j]<<",R2:"<<frame.pointsRadius2d[j]<<endl;
//                }

                if(frame.pointsRadius2d[i]<frame.pointsRadius2d[j])
                {
                    removedFlags[i]=true;
                }
                else removedFlags[j]=true;
            }
        }
    }

    //collect points
    vector<cv::Point2f> collectedPoints;
    for(int i=0;i<n;i++)
    {
        if(!removedFlags[i]) {
            collectedPoints.push_back(frame.pointsCenters2d[i]);
           // cv::circle(frame.img, frame.pointsCenters2d[i],2,Scalar(0,255,0),2);
        }
       // imshow("img_gray",frame.img);
    }

    if(collectedPoints.size()!=5)
    {
//        std::cout<<"No found 5 points\n";//<<"collectedPoints.size() = "<<collectedPoints.size()<<"\n";
        pi::timer.leave("ContourImpl::FindPoints");
        return -1;
        //std::cout<<"No found 5 points,but not stop!!!\n";
    }


    // 3. sort points
    /**
    1  2  3

    4     5
     */

    vector<int>               idx;
    idx.resize(5);
    vector<FPoint> collected;
    vector<int> index;
    index.resize(5);
    for(int i=0,iend=collectedPoints.size();i<iend;i++)
    {
        collected.push_back(FPoint(collectedPoints[i].x,collectedPoints[i].y));
    }

    isCollinear(collected,index);

    if(0){
        for (int i = 0; i < 5; i++)
        {
            std::stringstream ss;
            std::string text;
            ss<<i+1;
            ss>>text;
            cv::putText(frame.img,text,collectedPoints[index[i]],0,2,Scalar(0,255,255));
        }
    }
    for(int i=0;i<5;i++)
        frame.trackedPoints.push_back(collectedPoints[index[i]]);
    pi::timer.leave("ContourImpl::FindPoints");
    frame.state=VideoFrame::PointsFound;

    return 0;
}

int ContourImpl::ComputeLocation(VideoFrame& frame)
{

    if(!ledboard.get()) return -1;
    cv::Mat objPoints=ledboard->toMat();
    if(objPoints.empty()) return -2;

    pi::timer.enter("ContourImpl::ComputeLocation");
    cv::Mat K = cv::Mat::eye(3,3,CV_32F);
    cv::Mat disCoff= cv::Mat::zeros(5,1,CV_32F);

    cv::Mat imagePoints=cv::Mat(5,2,CV_32F,frame.trackedPoints.data()).clone();
    //convert to ideal camera
    for(int i=0;i<5;i++)
    {
        pi::Point3d ptw=camera.UnProject(pi::Point2d(imagePoints.at<float>(i,0),imagePoints.at<float>(i,1)));
        imagePoints.at<float>(i,0)=ptw.x;imagePoints.at<float>(i,1)=ptw.y;
    }

    cv::Mat R,T;
    if(cv::solvePnP(objPoints,imagePoints,K,disCoff,R,T))
    {

        pi::SE3d pose(pi::SO3d::exp(pi::Point3d(R.at<double>(0),R.at<double>(1),R.at<double>(2))),
                      pi::Point3d(T.at<double>(0),T.at<double>(1),T.at<double>(2)));
        frame.pose=pose.inverse();
        frame.state=0;

        pi::timer.leave("ContourImpl::ComputeLocation");
        return 0;
    }
    else
    {
        pi::timer.leave("ContourImpl::ComputeLocation");
        return -3;
    }
}
