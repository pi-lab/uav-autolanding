#include "FlightCommander.h"
#include "UAV.h"
#include "LedBoard.h"
#include <GL/gl.h>
#include <gui/gl/glHelper.h>

using namespace std;

void Rect3D::draw()
{
    glBegin(GL_TRIANGLES);
    glColor(c0);glVertex(p0);glColor(c1);glVertex(p1);glColor(c2);glVertex(p2);
    glColor(c0);glVertex(p0);glColor(c1);glVertex(p1);glColor(c3);glVertex(p3);
    glColor(c0);glVertex(p0);glColor(c3);glVertex(p3);glColor(c4);glVertex(p4);
    glColor(c0);glVertex(p0);glColor(c2);glVertex(p2);glColor(c4);glVertex(p4);
    if(0)
    {
        glVertex(p3);glVertex(p1);glVertex(p2);
        glVertex(p3);glVertex(p2);glVertex(p4);
    }
    glEnd();
}

class CommanderImpl
{
public:
    enum State{Waiting=0,       // Do nothing and just wait next cmd
               LandingGPS=1,    // Try to control the UAV with GPS infomation until the ledboard is tracked by the camera
               LandingFollow=2, // Now ledboard is found, adjust UAV to the safe area where ledboard can be captured robustly
               Landing=3,       // In the safe area, adjust to the best state and go to the end landing area slowly
               LandingLost=4,   // Losted, and try to go back to state LandingFollow automaticly, if not try use gps?
               LandingEnd=5,    // Here is too low to see the whole ledboard and we just keep landing slowly
               Error=6};        // Something really bad happened, what should we do?

    CommanderImpl(SPtr<UAV> _uav,pi::hardware::Camera _camera,SPtr<LedBoard> _ledboard)
        :uav(_uav),camera(_camera),ledboard(_ledboard),state(Waiting)
    {
        prepareData();
    }

    bool handleState(SPtr<VideoFrame> videoFrame);
    void draw();

private:
    bool prepareData();
    bool findUnProjectRect(double height,Rect2D& projRect);
    bool findRect3D(Rect2D sightRect,Rect3D& rect3d,const vector<pi::Point3d>& leds);

    void updateUavPoseFromFrame();
    bool inFollowRect();
    bool inLandRect();

    bool handleLandingGPS();
    bool handleLandingFollow();
    bool handleLanding();
    bool handleLost();

    bool remoteCamera(pi::Point3d target)
    {
        return uav->setTarget(camera2UAV*target,true);
    }

    bool remoteCamera(pi::Point2d target)
    {
        return remoteCamera(camera.UnProject(target));
    }

    bool remoteBoard(pi::Point3d target)
    {
        updateUavPoseFromFrame();
        if(uavPoseUpdated)
        {
            return uav->setTarget(uavPose.inverse()*target);
        }
    }

public:
    SPtr<UAV>            uav;
    pi::hardware::Camera camera;
    SPtr<LedBoard>       ledboard;
    State                state;

private:
    pi::SE3d             camera2UAV,uavPose;
    bool                 uavPoseUpdated;
    Rect3D               followRect,landRect;
    Rect2D               landImgRect,followImgRect;
    vector<pi::Point3d>  leds;

    State                lostFrom;
    SPtr<VideoFrame>     BeforeLost,curFrame;
};

bool CommanderImpl::findUnProjectRect(double height,Rect2D& projRect)
{
    Rect2D unProjRect;
    if(height<=0) return false;
    pi::SE3d w2c;
    w2c.get_rotation().FromAxis(pi::Point3d(1,0,0),3.1415926);
    w2c.get_translation().z=height;
    pi::Point3d axis=w2c.get_rotation()*camera.UnProject(projRect.lt);
    unProjRect.spread(pi::Point2d(axis.x*height,axis.y*height));
    axis=w2c.get_rotation()*camera.UnProject(projRect.rb);
    unProjRect.spread(pi::Point2d(axis.x*height,axis.y*height));
    projRect=unProjRect;
    return true;
}

bool CommanderImpl::findRect3D(Rect2D sightRect,Rect3D& rect3d,const vector<pi::Point3d>& leds)//find the Rect3D area where keeps ledBoard in sight
{
    if(!leds.size()) return false;

    double zmin=zmin<=0?0.01:rect3d.p0.z;
    double zmax=rect3d.p1.z;

    pi::SE3d w2c;
    w2c.get_rotation().FromAxis(pi::Point3d(1,0,0),3.1415926);

    // find zmin
    vector<pi::Point2d> ProjLeds;
    ProjLeds.resize(leds.size());
    for(;zmin<zmax;zmin+=0.01)
    {
        w2c.get_translation().z=zmin;
        int i=0;
        for(;i<leds.size();i++)
        {
            ProjLeds[i]=camera.Project(w2c*leds[i]);
            if(!sightRect.has(ProjLeds[i])) break;
        }
        if(i==leds.size()) //all points in sight
        {
            break;//
        }
    }
    rect3d.p0=pi::Point3d(0,0,zmin);
    Rect2D rectProj;
    // find upper rect2d
}

bool CommanderImpl::prepareData()
{
    if(state!=Waiting) return false;//should not call this during the landing
    if(!(uav.get()&&uav->valid())) {cerr<<"No valid UAV setted!\n";state=Error;return false;}
    if(!(ledboard.get())) {cerr<<"No valid LedBoard setted!\n";state=Error;return false;}
    if(!camera.isValid()) {cerr<<"No valid Camera   setted!\n";state=Error;return false;}

    // preparing landRect
    leds.resize(ledboard->size());
    Rect2D ledRect;
    for(int i=0,iend=ledboard->size();i<iend;i++)
    {
        leds[i]=pi::Point3d(ledboard->at(i).x,ledboard->at(i).y,ledboard->at(i).z);
        ledRect.spread(pi::Point2d(leds[i].x,leds[i].y));
    }

    camera2UAV.get_rotation().FromAxis(pi::Point3d(1,0,0),3.1415926);
    pi::SE3d w2c=camera2UAV;

    Rect2D landSight(pi::Point2d(0,0),pi::Point2d(camera.width(),camera.height()));
    Rect2D followSight(landSight.rb*0.15,landSight.rb*0.85);
    followImgRect=followSight;landImgRect=landSight;
    int mode=0;
    for(double zmin=0.01;zmin<3;zmin+=0.01)
    {
        w2c.get_translation().z=zmin;
        if(mode==0)//determin landRect.p0
        {
            int i=0;
            for(;i<leds.size();i++)
            {
                pi::Point2d proj=camera.Project(w2c*leds[i]);
                if(!landSight.has(proj)) break;
            }
            if(i==leds.size()) //out of sight land
            {
                landRect.p0=pi::Point3d(0,0,zmin);
                mode=1;//finished, then go to determin followRect.p0
            }
        }

        if(mode==1)//determin followRect.p0
        {
            int i=0;
            for(;i<leds.size();i++)
            {
                if(!followSight.has(camera.Project(w2c*leds[i]))) break;
            }
            if(i==leds.size()) //out of sight land
            {
                followRect.p0=pi::Point3d(0,0,zmin);
                mode=2;//finished, then go to determin followRect.p0
                break;
            }
        }
    }

    // now determin p1-p4 of landRect
    {
        double zmax=followRect.p0.z+0.08;
        findUnProjectRect(zmax,landSight);
        Rect2D moveLand(ledRect.rb-landSight.rb,ledRect.lt-landSight.lt);
        landRect.p1=pi::Point3d(moveLand.lt.x,moveLand.rb.y,zmax);
        landRect.p2=pi::Point3d(moveLand.rb.x,moveLand.rb.y,zmax);
        landRect.p3=pi::Point3d(moveLand.lt.x,moveLand.lt.y,zmax);
        landRect.p4=pi::Point3d(moveLand.rb.x,moveLand.lt.y,zmax);
        cout<<"LandRect:Zmin:"<<landRect.p0.z<<",Zmax:"<<landRect.p1.z<<",MoveLand:"<<moveLand<<endl;
    }

    // now determin p1-p4 of follow rect
    {
        double zmax=followRect.p0.z*3;
        findUnProjectRect(zmax,followSight);
        Rect2D moveLand(ledRect.rb-followSight.rb,ledRect.lt-followSight.lt);
        followRect.p1=pi::Point3d(moveLand.lt.x,moveLand.rb.y,zmax);
        followRect.p2=pi::Point3d(moveLand.rb.x,moveLand.rb.y,zmax);
        followRect.p3=pi::Point3d(moveLand.lt.x,moveLand.lt.y,zmax);
        followRect.p4=pi::Point3d(moveLand.rb.x,moveLand.lt.y,zmax);
        cout<<"FollowRect:Zmin:"<<landRect.p0.z<<",Zmax:"<<landRect.p1.z<<",MoveLand:"<<moveLand<<endl;
    }
}

void CommanderImpl::updateUavPoseFromFrame()
{
    if(uavPoseUpdated) return;
    if(curFrame->state==VideoFrame::Localized)
    {
        uavPose=curFrame->pose*camera2UAV.inverse();
        uavPoseUpdated=true;
    }
}

bool CommanderImpl::inFollowRect()
{
    updateUavPoseFromFrame();
    if(!uavPoseUpdated) return false;
    pi::Point3d trans=uavPose.get_translation();
    if(trans.z<followRect.p0.z||trans.z>followRect.p1.z) return false;
    vector<cv::Point2f> pts=curFrame->trackedPoints;
    if(!pts.size()) return false;
    int i=0;
    for(;i<pts.size();i++)
        if(!followImgRect.has(pi::Point2d(pts[i].x,pts[i].y)))
            return false;
    return true;
}

bool CommanderImpl::inLandRect()
{
    updateUavPoseFromFrame();
    if(!uavPoseUpdated) return false;
    pi::Point3d trans=uavPose.get_translation();
    if(trans.z<landRect.p0.z||trans.z>landRect.p1.z) return false;
    vector<cv::Point2f> pts=curFrame->trackedPoints;
    if(!pts.size()) return false;
    int i=0;
    for(;i<pts.size();i++)
        if(!landImgRect.has(pi::Point2d(pts[i].x,pts[i].y)))
            return false;
    return true;
}

bool CommanderImpl::handleLandingGPS()
{
    //check if uav reached followRect
    if(inFollowRect())
    {
        cout<<"Reached landingFollow at:"<<uavPose<<endl;
        state=LandingFollow;
        return handleLandingFollow();
    }

    if(curFrame->state==VideoFrame::Localized)
    {
        // just towarding to the followRect
        return remoteBoard(followRect.p0*2);
    }
    else if(curFrame->state==VideoFrame::ContoursFound||curFrame->state==VideoFrame::PointsFound)
    {
        // just towarding to the leds
        pi::Point2d center(0,0);
        if(curFrame->trackedPoints.size())
        {
            vector<cv::Point2f> pts=curFrame->trackedPoints;
            for(int i=0;i<pts.size();i++)
            {
                center+=pi::Point2d(pts[i].x,pts[i].y);
            }
            center/=pts.size();
        }
        else
        {
            vector<cv::Point2f> pts=curFrame->pointsCenters2d;
            vector<double>      rs= curFrame->pointsRadius2d;
            double weight=0;
            for(int i=0;i<pts.size();i++)
            {
                center+=pi::Point2d(pts[i].x,pts[i].y)*rs[i];
                weight+=rs[i];
            }
            center/=weight;
        }
        return remoteCamera(center);
    }
    //No GPS yet
    return false;
}

bool CommanderImpl::handleLandingFollow()
{
    //check if losted
    if(curFrame->state!=VideoFrame::Localized)
    {
        // lost!
        state=LandingLost;
        lostFrom=LandingFollow;
        return handleLost();
    }
    BeforeLost=curFrame;
    if(inFollowRect())
    {
        if(inLandRect())
        {
            // jumped in land rect Seccessfully!
            state=Landing;
            cout<<"Reached landRect at "<<uavPose<<endl;
            return handleLanding();
        }
        return remoteBoard(pi::Point3d(0,0,uavPose.get_translation().z-0.1));
    }
    else
    {
        // go back 2 follow rect
        return remoteBoard(pi::Point3d(0,0,uavPose.get_translation().z+0.05));
    }
    return false;
}

bool CommanderImpl::handleLanding()
{
    //just keep down, make sure its safe!
    return uav->setTarget(pi::Point3d(0,0,-0.1));
}

bool CommanderImpl::handleLost()
{
    //check if in followRect
    if(inFollowRect())
    {
        state=LandingFollow;
        return handleLandingFollow();
    }
    else if(curFrame->state==VideoFrame::Localized||(BeforeLost.get()&&curFrame->timestamp-BeforeLost->timestamp>6.))
    {
        // if lost for too long
        state=LandingGPS;
        return handleLandingGPS();
    }

    if(lostFrom==LandingFollow&&BeforeLost.get()&&curFrame->timestamp-BeforeLost->timestamp<1.)
    {
        pi::SE3d lostPose=BeforeLost->pose;
        pi::Point3d lostPosition=lostPose.get_translation();
        return uav->setTarget(pi::Point3d(-lostPosition.x,-lostPosition.y,0.2));
    }
    else
        return uav->setTarget(pi::Point3d(0,0,0.2));
}

bool CommanderImpl::handleState(SPtr<VideoFrame> frame)
{
    if(!frame.get()) return false;
    curFrame=frame;
    uavPoseUpdated=false;
    switch (state) {
    case Waiting:
    {
        if(uav->state()>0) state=LandingGPS;
        return true;
        break;
    }
    case LandingGPS:
    {
        return handleLandingGPS();
        break;
    }
    case LandingFollow:
    {
        return handleLandingFollow();
        break;
    }
    case Landing:
    {
        return handleLanding();
        break;
    }
    case LandingLost:
    {
        return handleLost();
    }
    case Error:
    {
        return false;
    }
    default:
        return false;
        break;
    }
}

void CommanderImpl::draw()
{
    if(state==Error) return;

    glColor4ub(0,255,100,100);
    followRect.draw();
    glColor4ub(255,100,0,100);
    landRect.draw();
}

FlightCommander::FlightCommander(SPtr<UAV> _uav,pi::hardware::Camera _camera,SPtr<LedBoard> _ledboard)
    :impl(new CommanderImpl(_uav,_camera,_ledboard))
{

}


bool FlightCommander::handleState(SPtr<VideoFrame> state)
{
    return impl->handleState(state);
}

void FlightCommander::draw()
{
    return impl->draw();
}
