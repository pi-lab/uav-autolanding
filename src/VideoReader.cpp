#include "VideoReader.h"
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

VideoReaderOpencv::VideoReaderOpencv(const string& filename):cap(filename)
{
    if(cap.isOpened())
    {
        cerr<<"VideoReaderOpencv: Opened file "<<filename<<" succeed!\n";
      //  cap.set(CV_CAP_PROP_POS_FRAMES,190);
    }
    else
    {
        cap.open(0);
        if(!cap.isOpened())
            cerr<<"Can't open file "<<filename<<".\n";
        else
        {
            cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
            cap.set(CV_CAP_PROP_FRAME_HEIGHT,480);
            cap.set(CV_CAP_PROP_FORMAT, CV_8UC1);
        }
    }
}

VideoReaderOpencv::VideoReaderOpencv(int video):cap(video)
{
    if(cap.isOpened())
    {
        cerr<<"VideoReaderOpencv: Opened file "<<video<<" succeed!\n";
        cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT,480);
        cap.set(CV_CAP_PROP_FORMAT, CV_8UC1);
    }
    else
    {
        cerr<<"Can't open file "<<video<<".\n";
    }
}

VideoReaderOpencv::~VideoReaderOpencv(){}

int VideoReaderOpencv::grabImage(VideoFrame& videoframe)
{
    // cout<<"============================================\n\n\n\n";
     cap>>videoframe.img;
    // std::cout<<"\ncurrent position = No."<<cap.get(CV_CAP_PROP_POS_FRAMES)<<" frame\n";
     if(videoframe.img.channels()==1) videoframe.img_gray=videoframe.img;
     else cv::cvtColor(videoframe.img,videoframe.img_gray,CV_BGR2GRAY);
 //   cv::GaussianBlur( src, videoframe.img, cv::Size( 7, 7 ), 0, 0 );
    if(videoframe.img_gray.empty()) return -1;
    else return 0;
}
