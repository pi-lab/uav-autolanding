#include "VideoFrame.h"
#include <base/Svar/Svar.h>

int VideoFrame::toStream(pi::RDataStream &ds)
{
    ds.clear();
    // set magic number & version number
    ds.setHeader(0x83F8, 1);

    ds.write(timestamp);
    ds.write((u_char*)&pose,sizeof(pose));
    ds.write(state);

    int tmp;

    if(svar.GetInt("VideoFrame.TransferImage",1))
    {
        ds.write(img.cols);
        ds.write(img.rows);
        tmp=img.type();ds.write(tmp);
        int size=img.cols*img.rows*img.elemSize();
//        std::cout<<"cols:"<<img.cols<<",rows:"<<img.rows<<",type:"<<img.type();
//        std::cout<<",elemSize:"<<img.elemSize()<<",size:"<<size<<std::endl;
        ds.write(img.data,size);
    }
    else
    {
        tmp=0;ds.write(tmp);
    }

    if(svar.GetInt("VideoFrame.TransferContour"))
    {
        tmp=contours.size();ds.write(tmp);
    }
    if(svar.GetInt("VideoFrame.TransferPoints"))
    {
        tmp=pointsCenters2d.size();ds.write(tmp);
    }

}

int VideoFrame::fromStream(pi::RDataStream &ds)
{
    uint        d_magic, d_ver;

    // rewind to begining
    ds.rewind();

    // get magic & version number
    ds.getHeader(d_magic, d_ver);

    if( d_magic != 0x83F8 ) {
        dbg_pe("Input data magic number error! %x\n", d_magic);
        return -1;
    }


    ds.read(timestamp);
    ds.read((u_char*)&pose,sizeof(pose));
    ds.read(state);

    int cols,rows,type,contoursNum;
    ds.read(cols);
    if(cols)
    {
        ds.read(rows);
        ds.read(type);
        if(cols*rows<0) return -2;
        img.create(rows,cols,type);
        int size=img.cols*img.rows*img.elemSize();
        std::cout<<"cols:"<<img.cols<<",rows:"<<img.rows<<",type:"<<img.type();
//        std::cout<<",elemSize:"<<img.elemSize()<<",size:"<<size<<std::endl;
        if(0 != ds.read(img.data,size))
            return -2;
    }

    ds.read(contoursNum);
    if(contoursNum)
    {
        contours.resize(contoursNum);
    }

    ds.read(contoursNum);
    if(contoursNum)
    {
        pointsCenters2d.resize(contoursNum);
    }

    return 0;
}
