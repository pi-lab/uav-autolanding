#ifndef SIMCAMERAREADER_H
#define SIMCAMERAREADER_H

#include "VideoReader.h"
#include <hardware/Camera/Cameras.h>

class SimCamera;
class UAV;

class SimCameraReader:public VideoReader
{
public:
    SimCameraReader(SPtr<UAV> _uav,pi::hardware::Camera camera);
    virtual bool valid();
    virtual int grabImage(VideoFrame& videoframe);

private:
    SPtr<SimCamera> impl;
};

#endif // SIMCAMERAREADER_H
