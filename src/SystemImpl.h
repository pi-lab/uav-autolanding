#ifndef SYSTEMIMPL_H
#define SYSTEMIMPL_H
#include "Tracker.h"

#include <gui/gl/PosedObject.h>

#include <base/system/thread/ThreadBase.h>
#include <base/Svar/Scommand.h>
#include <base/time/Global_Timer.h>
#include <network/InternetTransfer.h>
#include <gui/gl/Win3D.h>
#include <hardware/Camera/Cameras.h>
#include <base/time/DateTime.h>

class UAV;
class LedBoard;
class VideoReader;
class FlightCommander;

class SystemImpl:public pi::Thread,public pi::gl::Draw_Opengl,public pi::gl::EventHandle
{
public:
    SystemImpl();
    virtual void run();
    virtual void Draw_Something();
    virtual bool KeyPressHandle(void* arg);
    virtual bool MousePressHandle(void *);

    bool initLedBoard();
    bool initCameraRect();
    bool initUAV();

    void GenerateLedBoardTexture();
    void LedTrack();
    void ShowFrames();
    void SimUAV();
    void SetCameraTest();

private:
    SPtr<UAV>                 uav;
    SPtr<FlightCommander>     commander;
    SPtr<VideoReader>         video;

    pi::hardware::Camera      camera;
    SPtr<LedBoard>            ledboard;

    Tracker                   tracker_contour,tracker_direct;

    SPtr<VideoFrame>          lastFrame,curFrame;
    pi::MutexRW               lastFrameMutex;

    pi::gl::PosedObject       cameraRect;
    SPtr<pi::gl::Win3D>       win3d;

    SPtr<InternetTransfer<VideoFrame> > videoFrameTransfer;
    stringstream              info;

    int&                      pause;
    int&                      losted;
    int&                      showImage;
    int&                      pauseEveryFrame;
    int&                      cameraFollow;
};


#endif // SYSTEMIMPL_H
