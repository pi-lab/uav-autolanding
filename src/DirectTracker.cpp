#include "DirectTracker.h"

#include <base/Svar/Svar.h>
#include <../Thirdparty/levmar-2.6/src/levmar.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <base/time/Global_Timer.h>

using namespace std;

double WeightHuber(double r)
{
    double ret=1./ max(1., fabs(r));
    return ret*ret*ret;
}

double WeightWelsch(double r)
{
    return std::exp(-(r*r));
}

WeightedCircle::WeightedCircle(double radius,int type)
{
    if(radius<=0) return;

    double r2=radius*radius;
    double r_inv=1./radius;
    for(int x=-radius;x<=radius;x++)
        for(int y=-radius;y<=radius;y++)
        {
            double r=sqrt(x*x+y*y);
            if(r<=radius)
            {
                if(type==Huber)
                {
                    pts.push_back(WeightedPoint(x,y,WeightHuber(r)));
                }
                else if(type==WELSCH)
                {
                    pts.push_back(WeightedPoint(x,y,WeightWelsch(r)));
                }
            }
        }
    normalize();
}

void WeightedCircle::normalize()
{
    double sum=0;
    if(!pts.size()) return;
    for(int i=0;i<pts.size();i++)
        sum+=pts[i].weight;
    if(sum<=0) return;
    sum=pts.size()/sum;
    for(int i=0;i<pts.size();i++)
        pts[i].weight*=sum;
}

void errorFunction(double *para, double *err, int n_para, int n_err, void *data)
{
    DirectTracker* tracker=(DirectTracker*)data;
    tracker->errorFunc(para,err,n_para,n_err);
}

DirectTracker::DirectTracker():size(0),
    method(svar.GetInt("DirectTracker.Method",0)),
    win3d(svar.GetInt("Win3d.Enable",1)),
    verbose(svar.GetInt("DirectTracker.Verbose")),
    blurSize(svar.GetInt("DirectTracker.BlurSize",8)),
    smallDirectScale(svar.GetDouble("DirectTracker.SmallDirectScale",0.1))
{
    ledboard=SvarWithType<SPtr<LedBoard> >::instance()["LedBoardSPtr"];
    if(!ledboard.get())
    {
        cerr<<"No valid ledboard!\n";exit(0);
    }
    camera  =SvarWithType<pi::hardware::Camera>::instance()["MainCamera"];
    if(!camera.isValid())
    {
        cerr<<"No valid camera!\n";
        exit(0);
    }
    size=ledboard->size();
    ptsW.resize(size*5);
    double radius=ledboard->ledRadius;
    for(int i=0;i<size;i++)
    {
        pi::Point3d center(ledboard->at(i).x,ledboard->at(i).y,ledboard->at(i).z);
        ptsW[i*5]    =center;
        ptsW[i*5+1]  =center+pi::Point3d(-radius,0,0);
        ptsW[i*5+2]  =center+pi::Point3d(radius,0,0);
        ptsW[i*5+3]  =center+pi::Point3d(0,-radius,0);
        ptsW[i*5+4]  =center+pi::Point3d(0,radius,0);
    }

    //levmar initialization

    opts[0]=LM_INIT_MU; opts[1]=1E-15; opts[2]=1E-15; opts[3]=1E-20;
    opts[4]=svar.GetDouble("DirectTracker.Delta",5E-04);
    //    opts[4]= LM_DIFF_DELTA; // relevant only if the Jacobian is approximated using finite differences; specifies forward differencing
    // specifies central differencing to approximate Jacobian; more accurate but more expensive to compute!

    if(method==2)
    {
        weightedCircle=SPtr<WeightedCircle>(new WeightedCircle(svar.GetDouble("DirectTracker.WeightedRadius",10),
                                                               svar.GetInt("DirectTracker.WeightedType")));
    }
}

DirectTracker::~DirectTracker()
{
}

int DirectTracker::track(VideoFrame& videoFrame)
{
    int ret;
    this->frame=&videoFrame;
    pose=frame->pose;

    if(method==0)      ret=trackLines();
    else if(method==1) ret=trackCircle();
    else if(method==2) ret=trackWeightedCircle();
    else if(method==3) ret=trackSmallDirect();

    if(verbose)
    {
        printf("Levenberg-Marquardt returned %d in %g iter, reason %g\nSolution: ", ret, info[5], info[6]);
        for(int i=0; i<p.size(); ++i)
            printf("%.7g ", p.data[i]);
        printf("\n\nMinimization info:\n");
        for(int i=0; i<LM_INFO_SZ; ++i)
            printf("%g ", info[i]);
        printf("\n");
    }
    return ret;
}

void DirectTracker::errorFunc(double *para, double *err, int n_para, int n_err)
{
    if(method==0)      errorLines(para,err,n_para,n_err);
    else if(method==1) errorCircle(para,err,n_para,n_err);
    else if(method==2) errorWeightedCircle(para,err,n_para,n_err);
    else if(method==3) errorSmallDirect(para,err,n_para,n_err);
}

int DirectTracker::trackLines()
{
    errorVec.resize(size*4);
    int ret=dlevmar_dif(errorFunction, p.data, NULL, p.size(), size*4,
                        1000, opts, info, NULL, NULL, this);  // no Jacobian
    if(ret>=0&&error<150)
    {
        frame->pose=pi::SE3d::exp(p)*pose;
        return 0;
    }
    else return -1;
}

void DirectTracker::errorLines(double *para, double *err, int n_para, int n_err)
{
    if(blurSize>1)
//        cv::blur( frame->img_gray, frame->img_gray , cv::Size(blurSize,blurSize) );
    cv::GaussianBlur( frame->img_gray, frame->img_gray , cv::Size(blurSize,blurSize),blurSize,blurSize );
    error=0;
    pi::SE3d curpose=(pi::SE3d::exp(*(pi::Array_<double,6>*)para)*pose).inverse();
    for(int i=0,iend=size;i<iend;i++)//for every led
    {
        pmin=camera.Project(curpose*ptsW[i*5]);
        for(int j=0;j<4;j++)
        {
            int idx=i*4+j;
            pmax=camera.Project(curpose*ptsW[i*5+j+1]);
            pnow=pmax-pmin;
            double length=pnow.norm();
            pmax=pnow*(1./length);
            pnow=pmin;
            int nPoints=length+1;
            count=0;
            err[idx]=0;
            for(int k=0;k<nPoints;k++,pnow=pmax+pnow)
            {
                if(pnow.x<frame->img_gray.cols&&pnow.x>=0&&
                        pnow.y<frame->img_gray.rows&&pnow.y>=0)
                {
                    if(win3d)
                    {
                        cv::circle(frame->img,cv::Point(pnow.x,pnow.y),1,cv::Scalar(0,0,255));
                    }
                    uchar gray=frame->img_gray.data[((int)pnow.y)*frame->img_gray.cols+(int)pnow.x];//.at<uchar>(pnow.x,pnow.y);
                    err[idx]+=gray;
                    count++;
                }
            }
            if(count)
                err[idx]=255.-err[idx]/count;
            else err[idx]=255;

            errorVec[idx]=err[idx];
            error+=errorVec[idx];
        }
    }
    error=error*0.25/size;
//    cout<<"Pose:"<<pi::SE3d::exp(*(pi::Array_<double,6>*)para)<<",AverageError:"<<error<<endl;
}

int DirectTracker::trackCircle()
{
    errorVec.resize(size);
    int ret=dlevmar_dif(errorFunction, p.data, NULL, p.size(), size,
                        1000, opts, info, NULL, NULL, this);  // no Jacobian
    if(ret>=0&&error<150)
    {
        frame->pose=pi::SE3d::exp(p)*pose;
        frame->state=0;
        return 0;
    }
    else return -1;
}

void DirectTracker::errorCircle(double *para, double *err, int n_para, int n_err)
{
    error=0;
    pi::SE3d curpose=(pi::SE3d::exp(*(pi::Array_<double,6>*)para)*pose).inverse();
    double r=0;
    int idx=0;
    int w=frame->img_gray.cols;
    int h=frame->img_gray.rows;
    for(int i=0,iend=size;i<iend;i++,idx+=5)//for every led
    {
        err[i]=0;
        count=0;
        pnow=camera.Project(curpose*ptsW[idx]);
        pmin=camera.Project(curpose*ptsW[idx+1]);
        pmax=camera.Project(curpose*ptsW[idx+3]);
        r=(pmax-pmin).norm();
        for(int x=max((int)(pnow.x-r),0),xend=min((int)(pnow.x+r),w-1);x<xend;x++)
        for(int y=max((int)(pnow.y-r),0),yend=min((int)(pnow.y+r),h-1);y<yend;y++)
        {
            count++;
            err[i]+=frame->img_gray.data[w*y+x];
        }
        if(count)
            err[i]=255.-err[i]/count;
        else err[i]=255;
        if(win3d)
        {
            if(!n_para)
                cv::circle(frame->img,cv::Point(pnow.x,pnow.y),r,cv::Scalar(0,255,0));
            else
                cv::circle(frame->img,cv::Point(pnow.x,pnow.y),r,cv::Scalar(0,0,255));
        }
    }
    if(verbose)
    {
        for(int i=0;i<size;i++)
        {
            error+=err[i];
            cerr<<"pt"<<i<<":"<<err[i]<<",";
        }
        error/=size;
        cerr<<"average:"<<error<<endl;
    }
}

int DirectTracker::trackWeightedCircle()
{
    errorVec.resize(size);
    int ret=dlevmar_dif(errorFunction, p.data, NULL, p.size(), size,
                        1000, opts, info, NULL, NULL, this);  // no Jacobian
    if(ret!=LM_ERROR&&error<150)
    {
        frame->pose=pi::SE3d::exp(p)*pose;
        errorWeightedCircle(p.data,errorVec.data(),0,size);
        return 0;
    }
    else return -1;
}

void DirectTracker::errorWeightedCircle(double *para, double *err, int n_para, int n_err)
{
    if(!weightedCircle.get()) return;

    pi::timer.enter("DirectTracker::errorWeightedCircle");
    error=0;
    pi::SE3d curpose=(pi::SE3d::exp(*(pi::Array_<double,6>*)para)*pose).inverse();
    double r=svar.GetDouble("DirectTracker.WeightedRadius",10);
    int idx=0;
    int w=frame->img_gray.cols;
    int h=frame->img_gray.rows;
    std::vector<WeightedPoint> pts=weightedCircle->pts;
    for(int i=0,iend=size;i<iend;i++,idx+=5)//for every led
    {
        err[i]=0;
        double sumWeight=0;
        pnow=camera.Project(curpose*ptsW[idx]);
        for(int j=0,jend=pts.size();j<jend;j++)
        {
            int x=pnow.x+pts[j].x;
            int y=pnow.y+pts[j].y;
            if(x<0||x>=w||y<0||y>=h) continue;
            sumWeight+=pts[j].weight;
            err[i]+=(255-frame->img_gray.data[w*y+x])*pts[j].weight;
        }
        if(sumWeight)
            err[i]=err[i]/sumWeight;
        else err[i]=255;
        if(win3d&&verbose)
        {
            if(!n_para)
                cv::circle(frame->img,cv::Point(pnow.x,pnow.y),r,cv::Scalar(0,255,0),2);
            else
                cv::circle(frame->img,cv::Point(pnow.x,pnow.y),r,cv::Scalar(0,0,255));
        }
    }
    for(int i=0;i<size;i++)
    {
        error+=err[i];
    }
    error/=size;
    if(verbose)
    {
        for(int i=0;i<size;i++)
        {
            cerr<<"pt"<<i<<":"<<err[i]<<",";
        }
        cerr<<"average:"<<error<<endl;
    }
    pi::timer.leave("DirectTracker::errorWeightedCircle");
}

int DirectTracker::trackSmallDirect()
{
    cv::resize(frame->img_gray,frameImageSmall,
               cv::Size(frame->img_gray.cols*smallDirectScale,frame->img_gray.rows*smallDirectScale));
    cv::threshold(frameImageSmall,frameImageSmall,100,255,CV_THRESH_TOZERO);
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5));
    cv::dilate(frameImageSmall,frameImageSmall,element);
    errorVec.resize(frameImageSmall.cols*frameImageSmall.rows);
    int ret=dlevmar_dif(errorFunction, p.data, NULL, p.size(), errorVec.size(),
                        1000, opts, info, NULL, NULL, this);  // no Jacobian
    if(ret!=LM_ERROR&&error<150)
    {
        frame->pose=pi::SE3d::exp(p)*pose;
        return 0;
    }
    else return -1;
}

void DirectTracker::errorSmallDirect(double *para, double *err, int n_para, int n_err)
{
    pi::SE3d curpose=(pi::SE3d::exp(*(pi::Array_<double,6>*)para)*pose).inverse();
    double r=0;
    cv::Mat guessImg(frameImageSmall.rows,frameImageSmall.cols,frameImageSmall.type(),cv::Scalar(0));
    int idx=0;
    for(int i=0,iend=size;i<iend;i++,idx+=5)//for every led
    {
        pnow=camera.Project(curpose*ptsW[idx]);
        pmin=camera.Project(curpose*ptsW[idx+1]);
        pmax=camera.Project(curpose*ptsW[idx+3]);
        r=(pmax-pmin).norm();
        cv::circle(guessImg,cv::Point(pnow.x,pnow.y)*smallDirectScale,r*smallDirectScale-1,cv::Scalar(255),r*smallDirectScale);
    }
    for(int i=0,iend=guessImg.rows*guessImg.cols;i<iend;i++)
    {
        err[i]=guessImg.data[i]-frameImageSmall.data[i];
        errorVec[i]=fabs(err[i]);
        error+=errorVec[i];
    }
    if(!win3d)
    {
        cv::Mat residual(frameImageSmall.rows,frameImageSmall.cols,CV_64FC1,(uchar*)errorVec.data());

        cv::imshow("frameImageSmall",frameImageSmall);
        cv::imshow("guessImage",guessImg);
        cv::imshow("residual",residual*0.005);
        cv::waitKey(0);
    }
    error/=errorVec.size();
    cout<<"para:"<<*(pi::Array_<double,6>*)para<<",error:"<<error<<endl;
}
