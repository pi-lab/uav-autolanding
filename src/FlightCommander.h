#ifndef FLIGHTCOMMANDER_H
#define FLIGHTCOMMANDER_H

#include "VideoFrame.h"
#include <base/types/SPtr.h>
#include <hardware/Camera/Cameras.h>

/**
 * The flight commander decides the commands of uav from current state and makes sure that the camera always see the ledboard.
    IN>> pose,uav state,board,current videoframe;
    OUT<< commands;
    SUB TO:
    1. the ledboard keeps in sight of camera;
    2. uav land robust and safely
    3. robust against failures, take off and re-land?
**/
class UAV;
class LedBoard;
class CommanderImpl;


struct Rect3D
{
    /**                     z y
      p1----------p2        |/
       | \       / |        ---->x
      p3--\-----/-p4
        \  \   / /
          \ \/ /
            p0
    */
    Rect3D():c0(255,0,0),c1(0,255,0),c2(0,150,150),c3(0,150,150),c4(0,255,0){}
    void draw();
    pi::Point3f p0,p1,p2,p3,p4;
    pi::Point3ub c0,c1,c2,c3,c4;
};

struct Rect2D
{
    Rect2D(pi::Point2d _lt=pi::Point2d(0,0),pi::Point2d _rb=pi::Point2d(0,0)):lt(_lt),rb(_rb){}
    inline bool has(const pi::Point2d& pt){return pt.x>lt.x&&pt.x<rb.x&&pt.y>lt.y&&pt.y<rb.y;}
    inline bool has(const Rect2D& rect){return has(rect.lt)&&has(rect.rb);}
    inline bool spread(const pi::Point2d& pt)
    {
        bool changeX=false,changeY=false;
        if(pt.x<lt.x) {lt.x=pt.x;changeX=true;}
        if(pt.y<lt.y) {lt.y=pt.y;changeY=true;}
        if(pt.x>rb.x) {rb.x=pt.x;changeX=true;}
        if(pt.y>rb.y) {rb.y=pt.y;changeY=true;}
        return changeX||changeY;
    }
    inline bool shrink(const pi::Point2d& pt)
    {
        bool changeX=false,changeY=false;
        if(pt.x>lt.x) {lt.x=pt.x;changeX=true;}
        if(pt.y>lt.y) {lt.y=pt.y;changeY=true;}
        if(pt.x<rb.x) {rb.x=pt.x;changeX=true;}
        if(pt.y<rb.y) {rb.y=pt.y;changeY=true;}
        return changeX||changeY;
    }
    friend Rect2D operator -(Rect2D l,Rect2D r){return Rect2D(l.lt-r.lt,l.rb-r.rb);}

    friend inline std::ostream& operator <<(std::ostream& os,const Rect2D& r)
    {
        os<<"["<<r.lt<<" "<<r.rb<<"]";
        return os;
    }

    friend inline std::istream& operator >>(std::istream& is,Rect2D& r)
    {
        is>>r.lt>>r.rb;
        return is;
    }

    pi::Point2d lt,rb;
};

class FlightCommander
{
public:
    FlightCommander(SPtr<UAV> _uav,pi::hardware::Camera _camera,SPtr<LedBoard> _ledboard);

    bool handleState(SPtr<VideoFrame> state);
    void draw();

    SPtr<CommanderImpl>        impl;
};

#endif // FLIGHTCOMMANDER_H
