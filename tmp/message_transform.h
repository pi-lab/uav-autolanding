#ifndef MESSAGE_TRANSFORM_H
#define MESSAGE_TRANSFORM_H
#include "socket.h"
#include "flight_command.h"
#include <hardware/UART/UART.h>

class Message_Transform
{
public:
    Message_Transform(bool Is_UART_ = true,bool Is_SOCKET_ = true):Is_UART(Is_UART_),Is_SOCKET(Is_SOCKET_)
    {
        if(Is_UART){
            my_UART = new pi::UART;
            my_UART->port_name = "/dev/ttyAMA0";
            my_UART->baud_rate = 115200;
        }

        if(Is_SOCKET){
             my_SOCKET = new Socket_MYUDP;
        }

    }
    ~Message_Transform()
    {
        //close
        if(Is_UART){
            if(flight_cmd != NULL){
                delete flight_cmd;
            }//1
            if(my_UART != NULL){
                my_UART->close();
                delete my_UART ;
            }//2
        }

        if(Is_SOCKET){
            if(my_SOCKET != NULL){
                my_SOCKET->~Socket_MYUDP();
                delete my_SOCKET;
            }//1
        }

    }

    int start(const char *IP = "127.0.0.1");

    int send_to(pi::SE3d& current){
        if(Is_UART){
            UART_Send(current);
        }
        if(Is_SOCKET){
            UDP_Send(current);
        }
        return 0;
    }
    int reveive_from(pi::SE3d& current){
        if(Is_UART){
            UART_Rev(current);
        }
        if(Is_SOCKET){
            UDP_Rev(current);
        }
        return 0;
    }

protected:
    int UDP_Send(pi::SE3d& current);
    int UART_Send(pi::SE3d& current);

    int UDP_Rev(pi::SE3d& current);
    int UART_Rev(pi::SE3d& current);

    pi::UART*           my_UART;
    Socket_MYUDP*       my_SOCKET;
    flight_command*     flight_cmd;
    bool                Is_UART;
    bool                Is_SOCKET;


};

#endif // MESSAGE_TRANSFORM_H
