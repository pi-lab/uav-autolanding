#ifndef VIDEOFRAMES_H
#define VIDEOFRAMES_H
#include <vector>
#include <opencv2/core/core.hpp>
#include <base/types/SE3.h>
#include <base/Svar/DataStream.h>

struct VideoFrame
{
    enum State{PoseNotValid=-1,
               Localized=0,
               Handling=1,
               ContoursFound=2,
               PointsFound=3};
    VideoFrame():state(PoseNotValid){}

    // DataStream interface
    int toStream(pi::RDataStream &ds);
    int fromStream(pi::RDataStream &ds);

    double  timestamp;
    cv::Mat img,img_gray;

    std::vector<std::vector<cv::Point> > contours;

    std::vector<cv::Point2f> pointsCenters2d,trackedPoints;
    std::vector<double>      pointsRadius2d;

    pi::SE3d pose,groundPose;
    int      state;
};


#endif // VIDEOFRAMES_H
