#ifndef TRACKER_H
#define TRACKER_H
#include "VideoFrame.h"
#include <base/types/SPtr.h>

class TrackerImpl;

class Tracker
{
public:
    enum TrackMode{NoTracker,Contour,Direct};
    Tracker(TrackMode mode=NoTracker);

    int track(VideoFrame& frame);
private:
    SPtr<TrackerImpl> impl;
};

#endif // TRACKER_H
