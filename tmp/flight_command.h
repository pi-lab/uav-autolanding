#ifndef FLIGHT_COMMAND_H
#define FLIGHT_COMMAND_H
#include <base/types/SE3.h>
#include <opencv2/opencv.hpp>
#include <cmath>

#define landing_position_x 0.
#define landing_position_y 0.

class flight_command
{
public:
    flight_command(pi::SE3d pose,double maxstep = 0.1,double Saft_Height = 0.5) : current_pose(pose),safe_height(Saft_Height),height(pose.get_translation().z),max_step(maxstep){
        cv::Point3d temp(current_pose.get_translation().x,current_pose.get_translation().y,current_pose.get_translation().z);
        distance =  sqrt(temp.ddot(temp));
    }
    int generate_next_step(cv::Point3d& next);

protected:
    pi::SE3d current_pose;
    double height ;
    double distance ;
    double max_step;
    double safe_height;
};

#endif // FLIGHT_COMMAND_H
