#ifndef TRACKERIMPL_H
#define TRACKERIMPL_H
#include "VideoFrame.h"
class TrackerImpl
{
public:
    TrackerImpl();
    virtual ~TrackerImpl(){}

    virtual int track(VideoFrame& frame)=0;
};

#endif // TRACKERIMPL_H
