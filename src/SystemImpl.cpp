#include "SystemImpl.h"
#include "LedBoard.h"
#include "VideoReader.h"
#include "Simulation.h"
#include "FlightCommander.h"
#include "UAV.h"
#include "SimCameraReader.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <gui/controls/SvarTable.h>

void SystemHandle(void* ptr, std::string sCommand, std::string sParams)
{
    if(sCommand=="selectedName")
    {
        cout<<"Selected "<<svar.GetInt(sParams)<<endl;
    }
}


SystemImpl::SystemImpl()
    :camera(svar.GetString("Camera","CameraPI")),
      pause(svar.GetInt("Pause",0)),
      losted(svar.GetInt("Losted",1)),
      showImage(svar.GetInt("ShowImage",1)),
      pauseEveryFrame(svar.GetInt("PauseEveryFrame")),
      cameraFollow(svar.GetInt("Win3d.CameraFollow",1))
{
    scommand.RegisterCommand("selectedName",SystemHandle,this);
    //initialization things
    if(!initLedBoard())   return;
    if(!initCameraRect()) return;
    if(!initUAV())        return;

    tracker_contour=Tracker(Tracker::Contour);
    tracker_direct =Tracker(Tracker::Direct);

    string videoFile=svar.GetString("Video.File","/dev/video0");
    string videoType=svar.GetString("Video.Type","V4L");

    if(videoType=="OpenCV") video=SPtr<VideoReader>(new VideoReaderOpencv(videoFile));
    else if(videoType=="SimCamera") video=SPtr<VideoReader>(new SimCameraReader(uav,camera));
    if(!video.get()) cerr<<"No type of video:"<<videoType<<endl;
    if(!video->valid()) {cerr<<"Can't open video "<<videoFile<<endl;return;}
//   else if(videoType=="V4L") video=new VideoReaderV4L(videoFile);

    if(svar.GetInt("Win3d.Enable",1))
    {
        win3d=SPtr<pi::gl::Win3D>(new pi::gl::Win3D(NULL));
        SvarWithType<SPtr<pi::gl::Win3D> >::instance()["MainWin3D"]=win3d;
        start();
    }
    else
        run();

}

bool SystemImpl::initLedBoard()
{
    int LedBoardType=svar.GetInt("LedBoard.Type",1);
    if(LedBoardType==0)
    {
        //load ledboard
        cv::FileStorage fs(svar.GetString("LedBoard.File","test.yam"),
                           cv::FileStorage::READ);
        cv::Mat cameraMatrix,LED_Position;
        fs["cameraMatrix"] >> cameraMatrix;
        fs["LED_Position"] >> LED_Position;
        cout<<"LED_Position = \n"<<LED_Position<<"\n";
        ledboard=SPtr<LedBoard>(new LedBoard5(LED_Position));
    }
    else ledboard=SPtr<LedBoard>(new LedBoard5Ideal());

    SvarWithType<SPtr<LedBoard> >::instance()["LedBoardSPtr"]=ledboard;
    if(ledboard->size()!=5)
    {cerr<<"Not correct LedBoard!";stop();return false;}
    return true;
}

bool SystemImpl::initCameraRect()
{
    cout<<"CameraInfo:"<<camera.info()<<endl;
    if(!camera.isValid()) return false;
    SvarWithType<pi::hardware::Camera>::instance()["MainCamera"]=camera;
    vector<cv::Point3f> pts;
    double scale=svar.GetDouble("Camera.Scale",1);
    pi::Point3d pt3d=camera.UnProject(pi::Point2d(0,0));
    pts.push_back(cv::Point3f(pt3d.x,pt3d.y,pt3d.z)*scale);
    pt3d=camera.UnProject(pi::Point2d(camera.width(),0));
    pts.push_back(cv::Point3f(pt3d.x,pt3d.y,pt3d.z)*scale);
    pt3d=camera.UnProject(pi::Point2d(camera.width(),camera.height()));
    pts.push_back(cv::Point3f(pt3d.x,pt3d.y,pt3d.z)*scale);
    pt3d=camera.UnProject(pi::Point2d(0,camera.height()));
    pts.push_back(cv::Point3f(pt3d.x,pt3d.y,pt3d.z)*scale);
    cameraRect.setObject(pi::gl::GL_ObjectPtr(new CameraRect(pts)));
    return true;
}

bool SystemImpl::KeyPressHandle(void *arg) {
    QKeyEvent* e=(QKeyEvent*)arg;
    switch(e->key())
    {
    case Qt::Key_Space:
        return false;

    case Qt::Key_Escape:
        stop();
        return false;

    case Qt::Key_C:
        cameraFollow=!cameraFollow;
        if(cameraFollow)
        {
            pi::Point2d cxy=camera.Project(pi::Point3d(0,0,1));
            pi::Point2d fxy=camera.Project(pi::Point3d(1,1,1))-cxy;
            win3d->setCamera(640,480,fxy.x,fxy.y,cxy.x,cxy.y);
        }
        else
        {
            win3d->setCamera(640,480,-1,0,0,0);
            win3d->setBaseSize(640,480);
        }
        return true;
    case Qt::Key_G:

    {
        int& showgray=svar.GetInt("VideoFrame.ShowGray");
        showgray=1-showgray;
        if(win3d.get()) win3d->update();
    }
        return true;
    case Qt::Key_S:
    {
        static SvarWidget* svarWidget=new SvarWidget();
        svarWidget->show();
    }
        return true;

    case Qt::Key_P:
        pause=!pause;
        return true;

    case Qt::Key_M:
        svar.GetDouble("Image2Show.Scale",0.5)*=1.2;
        if(win3d.get()) win3d->update();
        return true;

    case Qt::Key_N:
        svar.GetDouble("Image2Show.Scale",0.5)*=0.8;
        if(win3d.get()) win3d->update();
        return true;

    case Qt::Key_L:
        svar.ParseLine("VirtualUAV setState 1");
        return true;

    default:
        svar.i["KeyPressMsg"] = e->key();
        return false;
    }
}

bool SystemImpl::MousePressHandle(void *arg)
{
    QMouseEvent* e=(QMouseEvent*)arg;
    if ((e->button() == Qt::LeftButton) && (e->modifiers() == Qt::ControlModifier))
    {
        win3d->setSelectRegionWidth(2);
        win3d->setSelectRegionHeight(2);
        win3d->select(e->pos());
        // Update display to show new selected objects
        win3d->update();
    }
}

void SystemImpl::run()
{
    string act=svar.GetString("Act","LedTrack");
    if(act=="LedTrack") LedTrack();
    else if(act=="ShowFrames") ShowFrames();
    else if(act=="GenerateLedBoardTexture") GenerateLedBoardTexture();
    else if(act=="SimUAV") SimUAV();
    else if(act=="SetCameraTest") SetCameraTest();
}

void SystemImpl::SetCameraTest()
{
    if(camera.CameraType()!="PinHole")
    {
        cerr<<camera.info()<<" is not Pinhole!\n";
        return;
    }
    if(!win3d.get()){cerr<<"No display!\n";return;}
    win3d->setSceneRadius(20);
    pi::SE3f cameraPose(0,0,-1,0,0,0,1);
    ledboard=SPtr<LedBoard>(new LedBoard);
    pi::Point3d lt=cameraPose*camera.UnProject(pi::Point2d(0,0));
    pi::Point3d rb=cameraPose*camera.UnProject(pi::Point2d(camera.width(),camera.height()));
    pi::Point3d lb=cameraPose*camera.UnProject(pi::Point2d(0,camera.height()));
    pi::Point3d rt=cameraPose*camera.UnProject(pi::Point2d(camera.width(),0));
    ledboard->push_back(cv::Point3f(lt.x,lt.y,lt.z));
    ledboard->push_back(cv::Point3f(rb.x,rb.y,rb.z));
    ledboard->push_back(cv::Point3f(lb.x,lb.y,lb.z));
    ledboard->push_back(cv::Point3f(rt.x,rt.y,rt.z));
    pi::Point2d cxy=camera.Project(pi::Point3d(0,0,1));
    pi::Point2d fxy=camera.Project(pi::Point3d(1,1,1))-cxy;
    win3d->setCamera(640,480,fxy.x,fxy.y,cxy.x,cxy.y);
    win3d->insert(new Grid(1,10));
    win3d->Show();
    win3d->SetDraw_Opengl(this);
    win3d->SetEventHandle(this);
    while(!shouldStop())
    {
        sleep(10);
        win3d->setPose(cameraPose);
    }

}

void SystemImpl::SimUAV()
{
    Simulation sim(win3d);
    sim.run();
}

void SystemImpl::ShowFrames()
{
    string nodeName=svar.GetString("InternetTransfer.NodeName","");
    if(nodeName.size())
    {
        videoFrameTransfer=SPtr<InternetTransfer<VideoFrame> >(new InternetTransfer<VideoFrame>());
        if( videoFrameTransfer->begin(nodeName) != 0 ) {
            videoFrameTransfer=SPtr<InternetTransfer<VideoFrame> >();
        }
    }

    if(!videoFrameTransfer.get())
    {
        cerr<<"Node did not started!\n";
        cerr<<"Master.ip:"<<svar.GetString("Master.ip","127.0.0.1")
              <<",Master.port:"<<svar.GetString("Master.port","30000")<<endl;
        return ;
    }

    //initialization things

    if(!initLedBoard()) return;
    if(!initCameraRect()) return;

    if(win3d.get())
        win3d->Show();

    pi::Rate rate(svar.GetDouble("Video.fps",30));
    pi::TicTac tictac;
    tictac.Tic();
    while(!shouldStop())
    {
        if(!pause)
        {
            if(videoFrameTransfer->size()>0)
            {
                pi::WriteMutex lock(lastFrameMutex);
                curFrame=videoFrameTransfer->pop();
                if(curFrame.get())
                {
//                    curFrame->img=imgfromQt();
                    lastFrame=curFrame;
                }
                else
                {
                    cout<<"Failed to get videoFrame.\n";
                }
                if(win3d.get()&&tictac.Tac()>0.033)
                {
                    win3d->setPose(lastFrame->pose);
                    win3d->update();
                    tictac.Tic();
                }
            }
            else
                continue;
            cout<<"Recv:"<<lastFrame->timestamp<<" "<<lastFrame->pose<<endl;

        }
        rate.sleep();
    }
}

bool SystemImpl::initUAV()
{
    if(!uav.get())
        uav=SPtr<UAV>(new UAV(svar.GetString("UAVName","NoUAV")));
    return true;
}

void SystemImpl::LedTrack()
{

    string nodeName=svar.GetString("InternetTransfer.NodeName","");
    if(nodeName.size())
    {
        videoFrameTransfer=SPtr<InternetTransfer<VideoFrame> >(new InternetTransfer<VideoFrame>());
        if( videoFrameTransfer->begin(nodeName) != 0 ) {
            videoFrameTransfer=SPtr<InternetTransfer<VideoFrame> >();
        }
    }

    if(uav.get()&&uav->valid())
        commander=SPtr<FlightCommander>(new FlightCommander(uav,camera,ledboard));

    //run main loop
    if(win3d.get())
    {
        win3d->SetDraw_Opengl(this);
        win3d->SetEventHandle(this);
        win3d->ShowStream(&info);
        win3d->Show();
    }
    pi::Rate rate(svar.GetDouble("Video.fps",30));
    pi::TicTac tictac;
    tictac.Tic();
    stringstream Info;
    pi::DateTime dataTime;
    while(!shouldStop())
    {
        if(!pause)
        {
            bool shouldUpdate=0;
            Info.str("");
            curFrame=SPtr<VideoFrame>(new VideoFrame);
            if(video->grabImage(*curFrame)<0) {sleep(10);continue;}
            int ret;
            {
                pi::ReadMutex lock(lastFrameMutex);
                if(lastFrame.get()) curFrame->pose=lastFrame->pose;
            }
            if(losted)
            {
                pi::timer.enter("ContourImpl::Track");
                pi::timer.disable();
                ret=tracker_contour.track(*curFrame);
                pi::timer.enable();
                pi::timer.leave("ContourImpl::Track");
            }
            else
            {
                pi::timer.enter("TrackDirect::Track");
                ret=tracker_direct.track(*curFrame);
                pi::timer.leave("TrackDirect::Track");
            }
            dataTime.fromTimeStampF(curFrame->timestamp);
            Info<<"T:"<<dataTime;
            if(ret==0)
            {
//                losted=false;
                //track is good

                Info<<",State:Good,Pose:"<<curFrame->pose<<endl;
                Info<<"GroundTrue:"<<curFrame->groundPose<<endl;
            }
            else
            {
                if(win3d.get())
                {
                    shouldUpdate=true;
                    if(svar.GetInt("PauseWhenLost"))
                        pause=true;
                }
                losted=true;
                Info<<",State:Losted\n";
            }
            {
                pi::WriteMutex lock(lastFrameMutex);
                lastFrame=curFrame;
            }

            info.str(Info.str());
            if(win3d.get()&&(tictac.Tac()>0.033||shouldUpdate))
            {
                if(cameraFollow)
                    win3d->setPose(lastFrame->pose);
                win3d->update();
                tictac.Tic();
            }
            if(commander.get())
                commander->handleState(curFrame);
            if(videoFrameTransfer.get())
                videoFrameTransfer->send(curFrame);
            if(pauseEveryFrame&&win3d.get()) pause=1;
        }
        rate.sleep();
    }
}

void SystemImpl::Draw_Something()
{
    pi::timer.enter("SystemImpl::Draw_Something");
//    glPushName(0);
    typedef uchar ru8;
    if(lastFrame.get())
    {
        if(lastFrame->state==0)
            cameraRect.setPose(lastFrame->pose);
        if(showImage)
        {
            std::vector<cv::Point2f> trackedPoints;
            cv::Mat img2show;
            {
                pi::ReadMutex lock(lastFrameMutex);
                if(svar.GetInt("VideoFrame.ShowGray",1))
                    img2show=lastFrame->img_gray;
                else
                    img2show=lastFrame->img;
                trackedPoints=lastFrame->trackedPoints;
            }

            if(!img2show.empty())
            {
                double scale=svar.GetDouble("Image2Show.Scale",0.5);
                if(scale!=1.)
                    cv::resize(img2show,img2show,cv::Size(img2show.cols*scale,img2show.rows*scale));

                if(img2show.channels()==1)
                    cv::cvtColor(img2show,img2show,CV_GRAY2BGR);

                if(svar.GetInt("VideoFrame.DrawPoints",1)&&trackedPoints.size()==5)
                {
//                    std::cout<<"\nputtext here:\n";
//                    for (int var = 0; var < 5; ++var) {
//                        std::cout<<trackedPoints[var].x<<","<<trackedPoints[var].y<<"\n";
//                    }

                    cv::circle(img2show,trackedPoints[0]*scale,5,cv::Scalar(0,255,255));
                    cv::putText(img2show,"1",trackedPoints[0]*scale,0,2*scale,cv::Scalar(0,255,255));
                    cv::putText(img2show,"2",trackedPoints[1]*scale,0,2*scale,cv::Scalar(0,255,255));
                    cv::putText(img2show,"3",trackedPoints[2]*scale,0,2*scale,cv::Scalar(0,255,255));
                    cv::putText(img2show,"4",trackedPoints[3]*scale,0,2*scale,cv::Scalar(0,255,255));
                    cv::putText(img2show,"5",trackedPoints[4]*scale,0,2*scale,cv::Scalar(0,255,255));
                }

                if( img2show.cols<win3d->width() && img2show.rows<win3d->height() ) {
                    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

                    int updownSwap = 1;
                    ru8 *buf = img2show.data;

                    // FIXME: up/down swap
                    if( updownSwap ) {
                        int w, h, c, lw;
                        ru8 *p1, *p2;

                        w = img2show.cols;
                        h = img2show.rows;
                        c = img2show.channels();
                        lw = w*c;

                        buf = new ru8[w*h*c];

                        p1 = img2show.data;
                        p2 = buf + (h-1)*lw;

                        for(int j=0; j<h; j++) {
                            memcpy(p2, p1, sizeof(ru8)*lw);

                            p1 += lw;
                            p2 -= lw;
                        }
                    }
                    glPushName(5);
                    glDrawPixels(img2show.cols, img2show.rows,
                                 GL_BGR, GL_UNSIGNED_BYTE,
                                 buf);
                    glPopName();
                    if( updownSwap ) delete [] buf;
                }
            }
        }
    }
    if(ledboard.get())
    {
        glPushName(4);
        ledboard->draw();
        glPopName();
    }
    glColor3ub(0,0,255);
    glPushName(7);
    cameraRect.draw();
    glPopName();
    if(uav.get()&&uav->type()=="Virtual")
    {
        if(lastFrame.get()&&0)
        {
            cameraRect.setPose(lastFrame->pose);
            glColor3ub(255,0,0);
            cameraRect.draw();
        }
        uav->draw();
        glEnable(GL_LIGHTING);
        glEnable(GL_BLEND); //enable
        glBlendFunc(GL_ONE,GL_SRC_ALPHA); //设置混合后的颜色
        glPushName(3);
        if(commander.get()) commander->draw();
        glPopName();
        glDisable(GL_BLEND);
        glDisable(GL_LIGHTING);
    }

    pi::timer.leave("SystemImpl::Draw_Something");
}

void SystemImpl::GenerateLedBoardTexture()
{
    int length=svar.GetInt("LedBoard.TextureSize",640);
    cv::Mat img(length,length,CV_8UC3,cv::Scalar(0,0,0));
    cv::circle(img,cv::Point(length*0.25,length*0.25),length/240,cv::Scalar(255,255,255),length/120);
    cv::circle(img,cv::Point(length*0.50,length*0.25),length/240,cv::Scalar(255,255,255),length/120);
    cv::circle(img,cv::Point(length*0.75,length*0.25),length/240,cv::Scalar(255,255,255),length/120);
    cv::circle(img,cv::Point(length*0.25,length*0.75),length/240,cv::Scalar(255,255,255),length/120);
    cv::circle(img,cv::Point(length*0.75,length*0.75),length/240,cv::Scalar(255,255,255),length/120);
    cv::imwrite(svar.GetString("LedBoard.File","ledboard.png"),img);

}
